$(document).ready(function() {
	// selectタグの自動生成
	$('select[name=addCategory]').on('change', function() {
		var category_val = $(this).val();
		$('select[name=addJunle] option').remove();
		$('<option value="" disabled selected>ジャンルを選択</option>').appendTo('select[name=addJunle]');
		$.ajax({
			url: "getJunle",
			type: "POST",
			dataType: 'json',
			data: {
				categoryId : category_val
			},
			success : function(data) {
				$('<option value="">すべて</option>').appendTo('select[name=addJunle]');
				$(data.junleList).each(function() {
					$('<option value="' + this.id + '">' + this.name + '</option>').appendTo('select[name=addJunle]');
				})
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('ジャンルエラー');
			}
		});
	});
	
});
