$(document).ready(function() {
	// ページング機能のjQuery 全件とってきたレンタル一覧を10件ずつ表示する
	var page = 0;
	function draw() {
		$('#page').html(page + 1);
		$('tr').hide();
		$('tr:first,tr:nth-child(2),tr:gt(' + page * 10 + '):lt(10)').show();
	};
	$(function() {
		$('#prev').click(function() {
			if (page > 0) {
				page--;
				draw();
			}
		});
		$('#next').click(function() {
			if (page < ($('tr').size() - 1) / 10 - 1) {
				page++;
				draw();
			}
		});
		draw();
	});


	// 検索をかけて検索結果文字列、テーブル生成
	function search(){
		var receipt_date_val = $('input[name=receiptDate]').val();
		var return_deadline_val = $('input[name=returnDeadline]').val();
		var return_datre_val = $('input[name=returnDate]').val();
		var lend_charge_val = $('input[name=lendCharge]').val();
		var return_charge_val = $('input[name=returnCharge]').val();
		var status_val = $('select[name=status]').val();
		$.ajax({
				type:'GET',
				url: 'getRental',
				dataType: 'json',
				async: false,
				data: {
					receiptDate : receipt_date_val,
					returnDeadline : return_deadline_val,
					returnDate : return_datre_val,
					lendCharge : lend_charge_val,
					returnCharge : return_charge_val,
					status : status_val
				},
				success: function(data){					
					$('table#table-result tr.tr-result').remove();
					$(data.rentalList).each(function(){
						$('<tr class="tr-result">' +
								'<td>' + this.id + '</td>' +
								'<td>' + this.receiptDate + '</td>' +
								'<td>' + this.returnDeadLine + '</td>' +
								'<td>' + this.returnDate + '</td>' +
								'<td>' + this.orderQuantity + '</td>' +
								'<td>' + this.lendCharge + '</td>' +
								'<td>' + this.returnCharge + '</td>' +
								'<td>' + this.status + '</td>' +
								'<td><a href="preRentalDetail?rental_id=' + this.id + '"><button class="btn btn-default" type="button">詳細</button></a></td>' + 
						'</tr>').appendTo('table#table-result');
					})
					page = 0;
					draw();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					 alert("リクエスト時になんらかのエラーが発生しました: " + textStatus + ":\n" + errorThrown);		
				}
		});
	};
	$(function() {
		search();
	});
	$('button#btn-search').click(function() {
		search();
	});
});