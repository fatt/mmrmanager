<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@
	page
	import="jp.co.mmr.dto.EmpDTO, java.util.ArrayList, java.sql.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>従業員マスタメンテナンス</title>

</head>
<body>

	<h1>従業員マスタメンテナンス</h1>

	<%
		if (null != session.getAttribute("edit_message")) {
	%>
	<div style="color: red"><%=session.getAttribute("edit_message")%>
	</div>
	<%
		}
	%>
	<%
		if (null != session.getAttribute("resetpassword_message")) {
	%>
	<div style="color: red"><%=session.getAttribute("resetpassword_message")%>
	</div>
	<%
		}
	%>
	<%
		if (null != session.getAttribute("delete_message")) {
	%>
	<div style="color: red"><%=session.getAttribute("delete_message")%>
	</div>
	<%
		}
	%>

	<table cellspacing=2 cellpadding=5 border=1>
		<tr>
			<th>ID</th>
			<th>氏名</th>
			<th>パスワード初期化</th>
			<th>権限</th>
			<th>編集</th>
			<th>削除</th>
		</tr>
		<c:forEach var="empList" items="${empList}">
			<tr id="${empList.empId}">
				<form action='EditEmp?empId=<c:out value="${empList.empId}"/>'
					method="post" name="confirm">
					<td align="center">
						<div class="predata_${empList.empId}"
							style='display: inline-block'>
							<c:out value="${empList.empId}" />
						</div>
						<div class="editmode_${empList.empId}" style='display: none'>
							<input type="text" name="editEmpId" value="${empList.empId}"
								maxlength=20>
						</div>
					</td>
					<td align="center">
						<div class="predata_${empList.empId}"
							style='display: inline-block'>
							<c:out value="${empList.empName}" />
						</div>
						<div class="editmode_${empList.empId}" style='display: none'>
							<input type="text" name="editEmpName" value="${empList.empName}"
								maxlength=20>
						</div>
					</td>
					<td align="center"><input type="button" value="リセット"
						class="resetpassword_button"></td>
					<td align="center">
						<div class="predata_${empList.empId}"
							style='display: inline-block'>
							<c:if test="${empList.empIsmaster == '1'}">
								<c:out value="管理者" />
							</c:if>
							<c:if test="${empList.empIsmaster != '1'}">
								<c:out value="従業員" />
							</c:if>
						</div>
						<div class="editmode_${empList.empId}" style='display: none'>

							<select name="editIsmaster">
								<option value="0">従業員</option>
								<option value="1">管理者</option>
							</select>

						</div>
					</td>

					<td align="center">
						<div class="predata_${empList.empId}"
							style='display: inline-block'>
							<input type="button" class="edit_button" value="修正">
						</div>
						<div class="editmode_${empList.empId}" style='display: none'>
							<input type="submit" value="確定" class="comfirm_button"> <input
								type="button" value="キャンセル" class="cancel_button">
						</div>
					</td>
				</form>
				<td align="center">
					<form action='deleteEmp?empId=<c:out value="${empList.empId}"/>'
						method="POST">
						<input type="submit" value="削除">
					</form>
				</td>

			</tr>
		</c:forEach>
		<tr>
			<form action="AddEmp" method="POST" name="add">
			<td>
				<input type="text" name="addEmpId" maxlength=20>
			</td>
			<td>
				<input type="text" name="addEmpName" maxlength=20>
			</td>
			<td>
				初期化パスワード
			</td>
			<td>
				<select name="addEmpIsmaster">
					<option value="0">従業員</option>
					<option value="1">管理者</option>
				</select>
			</td>
			<td>
				<input type="submit" value="追加">
			</td>
			</form>
		</tr>

	</table>


	<!-- jQuery読み込み -->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript">
/**
 * 編集ボタン押下⇒　編集モードに切り替える
 */
$('.edit_button').on('click',function(){
	var id = $(this).parents('tr').attr('id');
	$(".predata_" + id).toggle();
	$(".editmode_" + id).toggle();
});
/**
 * 確認ボタン押下　⇒　表示モードに切り替える
 */
$('.comfirm_button').on('click',function(){
	var id = $(this).parents('tr').attr('id');
	$(".predata_" + id).toggle();
	$(".editmode_" + id).toggle();
});
/**
 * キャンセルボタン押下　⇒　表示モードに切り替える
 */
$('.cancel_button').on('click',function(){
	var id = $(this).parents('tr').attr('id');
	$(".predata_" + id).toggle();
	$(".editmode_" + id).toggle();
});


/**
 * リセットボタン押下　⇒　resetEmppasswordServletに転送する
 */
 
 
/*  $('.resetpassword_button').on('click',function(){
	 	alert("hoge");
	 	$.get("ResetEmppassword",
	 			empId=$(this).parents('tr').attr('id'),
	 			function(empId){
	 			alert("従業員ID:" + empId + "のパスワードがリセットされました。");
	}); 
 }); 
 */ 
  $('.resetpassword_button').on('click',function(){
		alert($(this).parents('tr').attr('id'));
		$.post("ResetEmppassword");

	 	/* $.post("ResetEmppassword",
				$(this).parents('tr').attr('id'),
	 			function(empId){
	 			alert("従業員ID:" + empId + "のパスワードがリセットされました。"); */	 
  }); 
 	
/**
$('.resetpassword_button').on('click',function(){ 	
	$.post("ResetEmppassword",
}
			$(this).parents('tr').attr('id'),
			function(empId){
		alert("従業員ID:" + empId + "のパスワードがリセットされました。");
	}); 
	$('.reset_id').val($(this).parents('tr').attr('id'));
	$.post("ResetEmppassword",
			$(this).parents('tr').attr('id'),
			function(empId){
		alert("従業員ID:" + empId + "のパスワードがリセットされました。");
	});
});
**/
/**
	$.ajax({
		type:"POST",
		url:"ResetEmppassword",
		data:$(.)
		
		var id = $(this).parents('tr').attr('id');
		type:"POST",
		url:"EditEmp",
		data: $('.resetpassword_button').serialize(),
		dataType:"json",
		susccess: function(response){
			alert("パスワードリセットしました");
		}
	})
	});
**/
</script>
</body>
</html>