<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>パスワード変更</title>
	<script type = "text/javascript">
	<!--
	function password(){
		if (newpass.newpassword1.value == "" || newpass.newpassword2.value == ""){
			//新規パスワードまたは確認欄の入力が空欄の場合
			alert("新規パスワードと確認欄両方とも入力してください");
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
	<h1>管理システム</h1>
	<hr>
	<br>
	<h2>パスワード変更</h2>
	※初回ログイン時は、パスワードの変更をしてください。
	<form name="newpass" action="password" method="post">
		<h3>新規パスワード      	<input type=password name="newpassword1"> </h3>	
		<h3>確認		<input type=password name="newpassword2"> </h3>
		<br>
		<%--　エラーメッセージがあれば、表示する --%>
		<% if (null != session.getAttribute("password_error_message")) { %>
		<div style="color:red"><%= session.getAttribute ("password_error_message") %> </div>
		<% } %>
		<input type=submit value="ログイン" onclick="return password()">
	</form>
</body>
</html>