<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<title>管理システム -商品管理画面-</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="css/header.css" rel="stylesheet" type="text/css">
<style type="text/css">
hr.style-one {
    border: 0;
    height: 1px;
    background: #333;
    background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
    background-image:    -moz-linear-gradient(left, #ccc, #333, #ccc);
    background-image:     -ms-linear-gradient(left, #ccc, #333, #ccc);
    background-image:      -o-linear-gradient(left, #ccc, #333, #ccc);
}
.imagePreview {
    width: 100%;
    height: 300px;
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
}

</style>
</head>
<body>
	<div class="container" style="margin-top: 20px">
		<form action="merchandiseAddToEdit" method="post">
			<div>
				<div class="col-md-4">
					<p>商品No. <span id="addId"><c:out value="${requestScope.id}" /></span></p>
				</div>
				<div class="col-md-5">
					<input type="text" class="form-control" name="addTitle" />
				</div>
				<div class="col-md-3">
					<select class="form-control" name="addArrival">
						<option value="1" selected>新作</option>
						<option value="2">準新作</option>
						<option value="3">旧作</option>
					</select>
				</div>
			</div>
			<div class="col-md-12">
				<hr class="style-one">
			</div>
			<div>
				<!-- http://qiita.com/akatsuki174/items/53b7367b04ed0b066bbf -->
				<div class="col-md-4 col-md-offset-1">
					<div class="imagePreview"></div>
					<div class="input-group">
						<label class="input-group-btn"> <span
							class="btn btn-primary"> Choose File<input type="file"
								style="display: none" class="uploadFile">
						</span>
						</label> <input type="text" class="form-control" readonly="">
					</div>
				</div>
				<div class="col-md-5 col-md-offset-1">
					<div class="form-group">
						<label>アーティスト</label>
						<input type="text" class="form-control" name="addArtist" />
					</div>
					<div>
						<div class="form-group col-md-6">
							<label>カテゴリ</label>
							<select class="form-control" name="addCategory">
								<option value="1" selected>CD</option>
								<option value="2">DVD</option>
								<option value="3">Blu-ray</option>
							</select>
						</div>
						<div class="form-group col-md-6">
							<label>ジャンル</label>
							<select class="form-control" name="addJunle">
								<option value="" disabled selected style="display:none;">ジャンルを選択</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label>値段</label>
						<input class="form-control" type="text" name="addPrice" />
					</div>
					<div class="form-group">
						<label>その他</label>
						<textarea class="form-control" name="addExtra" rows="4"></textarea>
					</div>
				</div>
			</div>
			
			<div>
				<div class="col-md-6 col-md-offset-1">
					<label>個体識別番号: </label>
					<input class="form-control" type="text" name="addOriginNUm" />
				</div>
				<div class="col-md-4">
				</div>
			</div>
			
			<div>
				<div class="col-md-3 col-md-offset-9">
					<a href="merchandiseManager"><button type="button" class="btn btn-default">商品一覧画面へ</button></a>
					<button type="submit" id="btn-decide" class="btn btn-warning">確定</button>
				</div>
			</div>
		</form>
	</div>
	

	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/merchandiseAddDetail.js" type="text/javascript"></script>
</body>
</html>