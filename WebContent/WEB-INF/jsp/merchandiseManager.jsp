<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<title>管理システム -商品管理画面-</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="css/form_custom.css" rel="stylesheet" type="text/css">
<link href="css/header.css" rel="stylesheet" type="text/css">
<style type="text/css">
span {
	display: inline-block;
	width: 30%;
	text-align: center;
}


td.td-img, img.item-img {
	width: 100px;
}
-->
</style>

</head>
<body>
	<jsp:include page="header.jsp" flush="true" />

	<div class="container" style="margin-top: 20px">
		<div class="col-md-8 col-md-offset-2">
			<div class="form-inline">
				<div class="form-group">
					<select class="form-control" name="searchCategory">
						<option value='' disabled selected>カテゴリを選択</option>
						<option value=''>すべて</option>
						<option value="1">CD</option>
						<option value="2">DVD</option>
						<option value="3">Blu-ray</option>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" name="searchJunle">
						<option value='' disabled selected style='display:none;'>ジャンルを選択</option>
					</select>
				</div>
				<div class="form-group">
					<input class="form-control" type="text" name="searchName" placeholder="商品名" />
				</div>
				<button id="btn-search" class="btn btn-default">検索</button>
			</div>
		</div>
		<div class="col-md-2">
		</div>
		<div class="col-md-8 col-md-offset-2">
			<p id="text-size"></p>
			<p id="text-result"></p>
		</div>
		
		<table id="table-result" class="table">
			<tr>
				<th>行番号</th>
				<th></th>
				<th>タイトル</th>
				<th>新旧</th>
				<th>カテゴリ</th>
				<th>ジャンル</th>
				<th>アーティスト</th>
				<th>値段</th>
			</tr>
		</table>
	</div>
	<span id="prev"><c:out value="←" /></span>
	<span id="page"></span>
	<span id="next"><c:out value="→" /></span>
	
	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/merchandiseManager.js" type="text/javascript"></script>
</body>
<hr>
<footer><jsp:include page="footer.jsp" flush="true" /></footer>
</html>