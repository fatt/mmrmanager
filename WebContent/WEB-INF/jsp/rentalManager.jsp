<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>管理システム -商品一覧-</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/header.css">
<style type="text/css">
<!--
/*h1 {
	text-align: center;
}
.centering {
	width: 90%;
	mergin-top:10px;
	margin-right: auto;
	margin-left: auto;
}
table {
	width: 95%;
	margin-right: auto;
	margin-left: auto;
}*/
th.center {
	text-align: center;
}
td {
	text-align: center;
}
span {
	display: inline-block;
	width: 33%;
	text-align: center;
}
-->
</style>
</head>
<body>
	<jsp:include page="header.jsp" flush="true" />
	
	<div class="container">
		<div class="col-md-12" style="margin-bottom: 20px;"><h1 style="text-align: center;">レンタル一覧画面</h1></div>
		
		<table id="table-result" border="1" style="border-collapse: collapse">
			<tr>
				<th class="center col-md-1"></th>
				<th class="center col-md-2"><input style="width:100%;" type="date" name="receiptDate"　/></th>
				<th class="center col-md-2"><input style="width:100%;" type="date" name="receiptDate"　/></th>
				<th class="center col-md-2"><input style="width:100%;" type="date" name="receiptDate"　/></th>
				<th class="center col-md-1"></th>
				<th class="center col-md-1"><input style="width:100%;" type="text" name="lendCharge" placeholder="従業員ID"></th>
				<th class="center col-md-1"><input style="width:100%;" type="text" name="returnCharge" placeholder="従業員ID"></th>
				<th class="center col-md-1">
					<select width="100%;" name="status">
						<option value="">すべて</option>
						<option value="1"><c:out value="受付" /></option>
						<option value="2"><c:out value="貸出" /></option>
						<option value="3"><c:out value="返却" /></option>
						<option value="4"><c:out value="滞納" /></option>
					</select>
				</th>
				<th class="center col-md-1"><button id="btn-search" type="button" style="background:white;">検索</button></th>
			</tr>
			<tr>
				<th class="center"><c:out value="注文番号" /></th>
				<th class="center"><c:out value="注文受付日" /></th>
				<th class="center"><c:out value="返却予定日" /></th>
				<th class="center"><c:out value="返却日" /></th>
				<th class="center"><c:out value="数量" /></th>
				<th class="center"><c:out value="貸出担当" /></th>
				<th class="center"><c:out value="返却担当" /></th>
				<th class="center"><c:out value="ステータス" /></th>
				<th class="center"><c:out value="詳細" /></th>
			</tr>
		</table>
		<div style="mergin-top: 10px;">
			<span id="prev"><c:out value="←" /></span>
			<span id="page"></span>
			<span id="next"><c:out value="→" /></span>
		</div>
	</div>
</body>
<hr>
<footer><jsp:include page="footer.jsp" flush="true" /></footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/rentalManager.js"></script>
</html>