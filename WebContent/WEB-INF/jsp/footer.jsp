<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<body>
	<a style="color: black; text-decoration: underline;"
		href="rentalManager"><c:out value="レンタル一覧画面へ " /></a>
	<h4 style="text-align: right;">
		<c:out
			value="copyright © 2017 music & movie rental Co., Ltd All rights reserved." />
	</h4>
</body>
</html>