<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="jp.co.mmr.dto.EmpDTO, java.util.ArrayList, java.sql.Date"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<body>
	<div id="header">
		<!-- ロゴ -->
		<div id="header_left">
			<a href="rentalManager"><img class="header_img"
				src="img/MMR-logo-2.png" /></a>
		</div>
		<br>
		<div id="header_center">
			<!-- アクション：アカウント -->
			<div id="header_above">
				<p id="header_empName">
					<c:choose>
						<c:when test="${ sessionScope.loginEmp.empIsmaster == 1 }">
							<c:out value="管理者 ${sessionScope.loginEmp.empName} " />
						</c:when>
						<c:otherwise>
							<c:out value="name ${sessionScope.loginEmp.empName} " />
						</c:otherwise>
					</c:choose>
				</p>
				<a href="logout"><button type="button"><c:out value="ログアウト" /></button></a>
			</div>
		</div>
	</div>

	<c:if test="${sessionScope.loginEmp.empIsmaster == 1}">
		<nav class="navbar navbar-default">
		<div>
			<div id="gnavi" class="collapse navbar-collapse">
				<ul class="nav navbar-nav" style="width:100%;">
					<li><a href="preMerchandiseManager"><c:out value="商品マスタ" /></a></li>
				</ul>
			</div>
		</div>
		</nav>
	</c:if>
	
	<span id="header_error_message"></span>
	
</body>
</html>