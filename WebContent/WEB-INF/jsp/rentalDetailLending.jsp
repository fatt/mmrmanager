<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>管理システム</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/header.css">	
		<style type="text/css">
		<!--
			table th, table td {
			  text-align: center;
			}
		-->
		</style>
	</head>
	<body>
		<jsp:include page="header.jsp" flush="true"/>
		<div class="container">
			<div class="col-md-12"><h1 style="text-align: center">レンタル詳細<c:out value="" /></h1></div>
			<div>
				<div class="col-md-8 col-md-offset-1">
					<h4><c:out value="注文受付番号: ${sessionScope.rentalInfo.id}" /></h4>
					<h4><c:out value="注文ユーザーID: ${sessionScope.rentalInfo.userId }" /></h4>
					<h4><c:out value="注文受付日: ${ sessionScope.rentalInfo.receiptDate }" /></h4>
				</div>
				<div class="col-md-3"><h2><c:out value="貸出" /></h2></div>
			</div>
			<div class="col-md-4 col-md-offset-8"><h4><c:out value="返却日: ${ sessionScope.rentalInfo.returnDate }" /></h4></div>
			
			<div class="col-md-12" style="margin-bottom:10px;">
				<h4><c:out value="レンタル商品明細" /></h4>
				<form action="rentalDetailReturned" method="post">
					<table class="table-bordered" style="width:100%; margin-bottom:5px;">
						<tr>
							<th><c:out value="ステータス" /></th>
							<th><c:out value="商品番号" /></th>
							<th><c:out value="タイトル" /></th>
							<th><c:out value="新旧" /></th>
							<th><c:out value="アーティスト" /></th>
							<th><c:out value="カテゴリ" /></th>
							<th><c:out value="ジャンル" /></th>
							<th><c:out value="金額" /></th>
							<th><c:out value="返却担当" /></th>
							<th><c:out value="個体識別番号" /></th>
						</tr>
							<c:forEach var="detailInfo" items="${ sessionScope.detailList }" varStatus="vs">
								<tr>
				   					<td><c:out value="${ detailInfo.status }" /></td>
									<td><c:out value="${ detailInfo.merchandiseId }" /></td>
									<td><c:out value="${ detailInfo.title }" /></td>
									<td><c:out value="${ detailInfo.arrival }" /></td>
									<td><c:out value="${ detailInfo.artist }" /></td>
									<td><c:out value="${ detailInfo.category }" /></td>
									<td><c:out value="${ detailInfo.junle }" /></td>
									<td><c:out value="${ detailInfo.price }" /></td>
									<td><c:out value="${ detailInfo.returnCharge }" /></td>
									<td><c:out value="${ detailInfo.originNum }" /></td>
								</tr>
							</c:forEach>
					</table>
					<button class="btn btn-danger" type="submit" id="btn-rent" style="float:right;">返却</button>
				</form>
			</div>
			<div class="col-md-12"><h4 style="float:right;"><c:out value="合計点数: ${ sessionScope.rentalInfo.orderQuantity }点" /></h4></div>
			<div class="col-md-12"><h4 style="float:right; margin:0;"><c:out value="合計金額: ${ sessionScope.priceSum }円" /></h4></div>
				
			
			<div class="col-md-12"><a href="rentalManager" style="text-align:center; display:block;"><button class="btn btn-default">レンタル一覧へ</button></a></div>
		</div>
	</body>
	<hr>
	<footer><jsp:include page="footer.jsp" flush="true"/></footer>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
	<script type="text/javascript">
	</script>
</html>