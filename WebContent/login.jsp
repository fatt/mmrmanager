<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>管理システムログイン</title>
	<script type = "text/javascript">
	<!--
	function login(){
		if (fm.id.value == "" || fm.password.value == ""){
			//入力が空欄の場合
			alert("ログインIDとパスワードを入力してください");
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
	<h1>管理システム</h1>
	<hr>
	<br>
	<h2>ログイン</h2>
	<form name="fm" action="login" method="post">
		<h3>ID      	<input type=text name="id" maxlength=20> </h3>	
		<h3>パスワード		<input type=password name="password" maxlength=20> </h3>
		<br>
		<%--　エラーメッセージがあれば、表示する --%>
		<div style="color:red"><c:out value="${sessionScope.error_message}" /></div>
		<input type=submit value="ログイン" onclick="return login()">
	</form>
</body>
</html>