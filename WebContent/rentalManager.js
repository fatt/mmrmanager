$(document).ready(function() {
	// ページング機能のjQuery 全件とってきたレンタル一覧を10件ずつ表示する
	var page = 0;
	function draw() {
		$('#page').html(page + 1);
		$('tr').hide();
		$('tr:first,tr:gt(' + page * 10 + '):lt(10)').show();
	};
	$(function() {
		$('#prev').click(function() {
			if (page > 0) {
				page--;
				draw();
			}
		});
		$('#next').click(function() {
			if (page < ($('tr').size() - 1) / 10 - 1) {
				page++;
				draw();
			}
		});
		draw();
	});


	// 検索をかけて検索結果文字列、テーブル生成
	function serch(){
		var receipt_date_val = $('input[name=searchReceiptDate]').val();
		var return_deadline_val = $('input[name=searchReturnDeadline]').val();
		var return_datre_val = $('input[name=searchReturnDate]').val();
		var lend_charge_val = $('input[name=searchLendCharge]').val();
		var return_charge_val = $('input[name=searchReturnCharge]').val();
		var status_val = $('select[name=searchStatus]').val();
		
		$.ajax({
				type:'GET',
				url: 'searchRentalInfo',
				dataType: 'json',
				data: {
					receiptDate : receipt_date_val,
					returnDeadline : return_deadline_val,
					returnDate : return_datre_val,
					lendCharge : lend_charge_val,
					returnCharge : return_charge_val,
					status : status_val
				},
				success: function(data){
					$('p#text-size').text('');
					$('p#text-size').append(data.listSize + '件見つかりました');
					$('p#text-result').text('');
					if(receipt_date_val || return_deadline_val || return_datre_val || lend_charge_val || return_charge_val || status_val){
						$('p#text-result').append('検索条件');
					}
					if(receipt_date_val){
						$('p#text-result').append(' 注文受付日："' + data.searchReceiptDate + '"');
					}
					if(return_deadline_val){
						$('p#text-result').append(' 返却予定日："' + data.searchReturnDeadline + '"');
					}
					if(return_datre_val){
						$('p#text-result').append(' 返却日："' + data.searchReturnDate + '"');
					}
					if(lend_charge_val){
						$('p#text-result').append(' 貸出担当："' + data.searchLendCharge + '"');
					}
					if(return_charge_val){
						$('p#text-result').append(' 返却担当："' + data.searchReturnCharge + '"');
					}
					if(status_val){
						$('p#text-result').append(' ステータス："' + data.searchStatus + '"');
					}
					
					$('table#table-result tr.tr-result').remove();
					$(data.rentalList).each(function(){
						$('<tr class="tr-result">' +
								'<td>' + this.id + '</td>' +
								'<td>' + this.recipetDate + '</td>' +
								'<td>' + this.returnDeadline + '</td>' +
								'<td>' + this.returnDate + '</td>' +
								'<td>' + this.orderQuantity + '</td>' +
								'<td>' + this.lendChargeId + '</td>' +
								'<td>' + this.returnChargeId + '</td>' +
								'<td>' + this.statusId + '</td>' +
								'<td><a href="rentalDetail?id=' + this.id + '"><button class="btn btn-default" type="button">詳細</button></a></td>' + 
						'</tr>').appendTo('table#table-result');
					})
					page = 0;
					draw();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					 alert("リクエスト時になんらかのエラーが発生しました: " + textStatus + ":\n" + errorThrown);		
				}
		});
	};
	$(function() {
		search();
	});
	$('button#btn-search').click(function() {
		search();
	});
});