package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.DataSourceManager;

@WebServlet("/merchandiseFileUpload")
public class MerchandiseFileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try (Connection con = DataSourceManager.getConnection()) {
    		MerchandiseDTO dto = new MerchandiseDTO();
    		for(Part p :request.getParts()){
    			if ("image".equals(p.getName())) {
    				dto.setImage(p.getInputStream());
    			} else if("name".equals(p.getName())) {
    				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
    				dto.setName(br.readLine());
    			} else if("category_id".equals(p.getName())) {
    				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
    				dto.setCategoryid(Integer.parseInt(br.readLine()));
    			} else if("junle_id".equals(p.getName())) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    dto.setJunleid(Integer.parseInt(br.readLine()));
                } else if("artist_id".equals(p.getName())) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    dto.setArtistid(Integer.parseInt(br.readLine()));
                } else if("price".equals(p.getName())) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    dto.setPrice(Integer.parseInt(br.readLine()));
                } else if("arrival_id".equals(p.getName())) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    dto.setArrivalid(Integer.parseInt(br.readLine()));
                } else if("origin_num".equals(p.getName())) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    dto.setOriginnum(Integer.parseInt(br.readLine()));
                }
    		}
    		
    		MerchandiseDAO dao = new MerchandiseDAO(con);
    		if (1 == dao.setMerchandise(dto)) {
    		    System.out.println("success!!");
            } else {
                System.out.println("error!!");
            }
    	}catch(NumberFormatException | SQLException | NamingException e){
    		e.printStackTrace();
    	}
	}
}