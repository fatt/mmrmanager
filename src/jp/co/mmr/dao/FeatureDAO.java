package jp.co.mmr.dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import jp.co.mmr.dto.FeatureDTO;

/**
 * 特集データDAOクラス
 * @author RyosukeOmori
 *
 */
public class FeatureDAO {
    
    private Connection con;
    
    public FeatureDAO(Connection con) {
        this.con = con;
    }
    
    /**
     * Featureテーブル内にある全ての要素をFeatureDTOオブジェクトとして取得する。
     * @return 取得したFeatureDTOリスト
     * @throws SQLException
     */
    public ArrayList<FeatureDTO> getAllFeatureList() throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("select * from feature");
        
        ArrayList<FeatureDTO> list = new ArrayList<>();
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                FeatureDTO featureDto = new FeatureDTO();
                featureDto.setId(rs.getInt("ID"));
                featureDto.setTitle(rs.getString("TITLE"));
                Blob img = rs.getBlob("IMAGE");
                if (img != null) {
                    featureDto.setImage(img.getBinaryStream());
                }
                featureDto.setPeriod(rs.getInt("PERIOD"));
                list.add(featureDto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * 引数に渡された指定月に該当するFeatureDTOをFeatureテーブルから取得する
     * ex) list = dao.getMonthlyFeatureList(201706); //2017年6月の特集を取得
     * @param period 取得したい特集データの期間
     * @return 条件に該当するFeatureDTOリスト
     * @throws SQLException
     */
    public ArrayList<FeatureDTO> getMonthlyFeatureList(int period) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        feature");
        sb.append("    WHERE");
        sb.append("        period = ?");
        
        ArrayList<FeatureDTO> list = new ArrayList<>();
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setInt(1, period);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                FeatureDTO featureDto = new FeatureDTO();
                featureDto.setId(rs.getInt("ID"));
                featureDto.setTitle(rs.getString("TITLE"));
                Blob img = rs.getBlob("IMAGE");
                if (img != null) {
                    featureDto.setImage(img.getBinaryStream());
                }
                featureDto.setPeriod(rs.getInt("PERIOD"));
                list.add(featureDto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * 特集IDから該当する特集DTOを取得する
     * @param id 取得したい特集のID
     * @return 取得した特集データ又はnull
     * @throws SQLException
     */
    public FeatureDTO getFeatureById(int id) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        feature");
        sb.append("    WHERE");
        sb.append("        id = ?");
        
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                FeatureDTO featureDto = new FeatureDTO();
                featureDto.setId(rs.getInt("ID"));
                featureDto.setTitle(rs.getString("TITLE"));
                Blob img = rs.getBlob("IMAGE");
                if (img != null) {
                    featureDto.setImage(img.getBinaryStream());
                }
                featureDto.setPeriod(rs.getInt("PERIOD"));
                return featureDto;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
    /**
     * 特集IDから該当する画像データを取得する
     * @param id 取得したい画像データの特集ID
     * @return 取得した画像データ
     * @throws IOException
     * @throws SQLException
     */
    public BufferedImage getImageById(int id) throws IOException, SQLException {
        FeatureDTO dto = getFeatureById(id);
        InputStream is = dto.getImage();
        BufferedInputStream bis = new BufferedInputStream(is);
        return ImageIO.read(bis);
    }
    
    /**
     * Featureテーブルに引数に渡した特集DTOを格納する
     * @param dto 追加したい特集データ
     * @return 追加したら1,出来なければ0
     * @throws SQLException
     */
    public int setFeature(FeatureDTO dto) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("INSERT");
        sb.append("    INTO");
        sb.append("        feature(");
        sb.append("            title");
        sb.append("            ,image");
        sb.append("            ,period");
        sb.append("        )");
        sb.append("    VALUES");
        sb.append("        (");
        sb.append("            ?");
        sb.append("            ,?");
        sb.append("            ,?");
        sb.append("        )");

        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setString(1, dto.getTitle());
            ps.setBlob(2, dto.getImage());
            ps.setInt(3, dto.getPeriod());
            
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

}