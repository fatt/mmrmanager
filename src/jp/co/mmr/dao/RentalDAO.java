package jp.co.mmr.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;

import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.dto.RentalInfoDTO;
import jp.co.mmr.util.DateUtil;

public class RentalDAO {

	// �R�l�N�V�����쐬����
	private Connection conn;

	public RentalDAO(Connection conn) {
		this.conn = conn;
	}

	/**
	 * �����f�[�^�ꗗ���� (�]�ƈ��T�C�g)
	 * 
	 * @throws NamingException
	 */
	public ArrayList<RentalDTO> selectRentalInfo() throws SQLException, NamingException {
//		SQL文　ステータス順＞受付時間順で並ぶ
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        rental_detail");
		sb.append("        ,status");
		sb.append("    ORDER BY");
		sb.append("        status.turn");
		sb.append("        ,rental_detail.receipt_date DESC");

		// SQL���玝���Ă����f�[�^�i�[�p���X�g�쐬
		//ArrayList<RentalFormDTO> list = new ArrayList<>();
		 ArrayList<RentalDTO> list = new ArrayList<>();
		try (PreparedStatement ps = conn.prepareStatement(sb.toString());) {
			// SQL���s
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				RentalDTO rentalRowData = new RentalDTO();
				rentalRowData.setId(rs.getInt("id"));
				rentalRowData.setReceiptDate(rs.getDate("receipt_date"));
				rentalRowData.setReturnDeadline(rs.getDate("return_deadline"));
				rentalRowData.setReturnDate(rs.getDate("return_date"));
				rentalRowData.setOrderQuantity(rs.getInt("order_quantity"));
				rentalRowData.setLendChargeId(rs.getString("lend_charge_id"));
				rentalRowData.setReturnChargeId(rs.getString("return_charge_id"));
				rentalRowData.setStatusId(rs.getInt("status_id"));
				list.add(rentalRowData);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ArrayList<RentalDTO> getSortRentalInfo(Date receiptDate, Date returnDeadLine, Date returnDate, String lendCharge, String returnCharge, int status) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        rental_detail");
		sb.append("        ,status");
		sb.append("    WHERE");
		sb.append("        rental_detail.status_id = status.id");
		
		if (receiptDate != null) {
			sb.append("        AND receipt_date BETWEEN ? and ?");
		}
		if (returnDate != null) {
			sb.append("        AND return_date BETWEEN ? and ?");
		}
		if (returnDeadLine != null) {
			sb.append("        AND return_deadline BETWEEN ? and ?");
		}
		if (lendCharge != null ){
			sb.append("        AND lend_charge_id LIKE ?");
		}
		if (returnCharge != null) {
			sb.append("        AND return_charge_id LIKE ?");
		}
		if (status > 0) {
			sb.append("        AND status_id = ?");
		}
		sb.append("    ORDER BY");
		sb.append("        status.turn");
		sb.append("        ,rental_detail.receipt_date DESC;");

		// SQL���玝���Ă����f�[�^�i�[�p���X�g�쐬
		ArrayList<RentalDTO> list = new ArrayList<>();
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			int count = 0;
			if (receiptDate != null) {
				count += 1;
                ps.setDate(count, receiptDate);
				count += 1;
                ps.setDate(count, DateUtil.addSQLDate(receiptDate, 1));
			}
			if (returnDate != null) {
				count += 1;
				ps.setDate(count, returnDate);
				count += 1;
                ps.setDate(count, DateUtil.addSQLDate(returnDate, 1));
			}
			if (returnDeadLine != null) {
				count += 1;
				ps.setDate(count, returnDeadLine);
				count += 1;
                ps.setDate(count, DateUtil.addSQLDate(returnDeadLine, 1));
			}
			if (lendCharge != null) {
				count += 1;
				ps.setString(count, '%' + lendCharge + '%');
			}
			if (returnCharge != null) {
				count += 1;
				ps.setString(count, '%' + returnCharge + '%');
			}
			if (status > 0) {
				count += 1;
				ps.setInt(count, status);
			}
			// SQL���s
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				RentalDTO rentalRowData = new RentalDTO();
				rentalRowData.setId(rs.getInt("id"));
				rentalRowData.setReceiptDate(rs.getDate("receipt_date"));
				rentalRowData.setReturnDeadline(rs.getDate("return_deadline"));
				rentalRowData.setReturnDate(rs.getDate("return_date"));
				rentalRowData.setOrderQuantity(rs.getInt("order_quantity"));
				rentalRowData.setLendChargeId(rs.getString("lend_charge_id"));
				rentalRowData.setReturnChargeId(rs.getString("return_charge_id"));
				rentalRowData.setStatusId(rs.getInt("status_id"));
				list.add(rentalRowData);
				System.out.println(rentalRowData.getId());
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}
	

	public RentalDTO getRentalById(int id) throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        rental_detail");
		sb.append("    WHERE");
		sb.append("        id = ?");
		try(PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				RentalDTO rentalRowData = new RentalDTO();
				rentalRowData.setId(rs.getInt("id"));
				rentalRowData.setUserId(rs.getInt("user_id"));
				rentalRowData.setReceiptDate(rs.getDate("receipt_date"));
				rentalRowData.setReturnDeadline(rs.getDate("return_deadline"));
				rentalRowData.setReturnDate(rs.getDate("return_date"));
				rentalRowData.setOrderQuantity(rs.getInt("order_quantity"));
				rentalRowData.setLendChargeId(rs.getString("lend_charge_id"));
				rentalRowData.setReturnChargeId(rs.getString("return_charge_id"));
				rentalRowData.setStatusId(rs.getInt("status_id"));
				return rentalRowData;
			}			
		} catch (SQLException e) {
			throw e;
		}
		return null;
	}
	
	public ArrayList<RentalInfoDTO> getRentalDetailByRentalID(int rentalId)
			throws SQLException{
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT        ");
		sb.append("		rental_detail.id        ");
		sb.append("		,rental_detail.status_id");
		sb.append("        ,status.name");
		sb.append("        ,rental_detail.receipt_date");
		sb.append("        ,rental_detail.return_deadline");
		sb.append("        ,rental_detail.return_date");
		sb.append("		,merchandise.id");
		sb.append("		,merchandise.name");
		sb.append("		,merchandise.arrival_id");
		sb.append("		,arrival.name");
		sb.append("		,merchandise.artist_id");
		sb.append("		,artist.name");
		sb.append("		,merchandise.category_id");
		sb.append("		,category.name");
		sb.append("		,merchandise.junle_id");
		sb.append("		,junle.name");
		sb.append("		,purchase_detail.merchandise_id");
		sb.append("		,merchandise.price");
		sb.append("		,rental_detail.lend_charge_id");
		sb.append("		,rental_detail.return_charge_id");
		sb.append("		,purchase_detail.return_charge_id");
		sb.append("		,purchase_detail.origin_num");
		sb.append("	FROM");
		sb.append("		purchase_detail");
		sb.append("	join");
		sb.append("		rental_detail");
		sb.append("	ON");
		sb.append("		purchase_detail.rental_id = rental_detail.id");
		sb.append("	join");
		sb.append("		status");
		sb.append("	ON");
		sb.append("		rental_detail.status_id = status.id");
		sb.append("	JOIN");
		sb.append("		merchandise");
		sb.append("	ON ");
		sb.append("		purchase_detail.merchandise_id = merchandise.id");
		sb.append("	JOIN ");
		sb.append("		arrival");
		sb.append("	ON ");
		sb.append("		merchandise.arrival_id = arrival.id        ");
		sb.append("	JOIN ");
		sb.append("		artist");
		sb.append("	ON");
		sb.append("		merchandise.artist_id = artist.id");
		sb.append("	JOIN ");
		sb.append("		category");
		sb.append("	ON	");
		sb.append("		merchandise.category_id = category.id");
		sb.append("	JOIN ");
		sb.append("		junle");
		sb.append("	ON ");
		sb.append("		merchandise.junle_id = junle.id");
		sb.append("	where ");
		sb.append("		rental_detail.id = ?;");

		// �f�[�^���i�[����I�u�W�F�N�g���i�[���郊�X�g�̐���
		ArrayList<RentalInfoDTO> list = new ArrayList<>();
		try(PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setInt(1, rentalId);
			
			// SQL���s
			ResultSet rs = ps.executeQuery();
	
			while (rs.next()) {
				//�@���X�g�Ɋi�[����f�[�^�����o���A�I�u�W�F�N�g�Ɋi�[
				RentalInfoDTO rentalInfoDTO = new RentalInfoDTO();
				rentalInfoDTO.setStatus(rs.getString("status.name"));
				rentalInfoDTO.setReceiptDate(rs.getDate("rental_detail.receipt_date"));
				rentalInfoDTO.setReturnDeadline(rs.getDate("rental_detail.return_deadline"));
				rentalInfoDTO.setReturnDate(rs.getDate("rental_detail.return_date"));
				rentalInfoDTO.setMerchandiseId(rs.getInt("merchandise.id"));
				rentalInfoDTO.setTitle(rs.getString("merchandise.name"));
				rentalInfoDTO.setArrival(rs.getString("arrival.name"));
				rentalInfoDTO.setArtist(rs.getString("artist.name"));
				rentalInfoDTO.setCategory(rs.getString("category.name"));
				rentalInfoDTO.setJunle(rs.getString("junle.name"));
				rentalInfoDTO.setPrice(rs.getInt("merchandise.price"));
				rentalInfoDTO.setReturnCharge(rs.getString("rental_detail.return_charge_id"));
				rentalInfoDTO.setReturnCharge(rs.getString("purchase_detail.return_charge_id"));
				rentalInfoDTO.setLendCharge(rs.getString("rental_detail.lend_charge_id"));
				rentalInfoDTO.setOriginNum(rs.getInt("purchase_detail.origin_num"));

				//�@���X�g�ɃI�u�W�F�N�g���i�[
				list.add(rentalInfoDTO);
			}
			return list;

		}catch (SQLException e) {
			throw e;
		}
	}
	
	// 注文詳細貸出時にしか使わなそうな汎用性の無いメソッド。なんで俺がやらなあかんねんオラオラオラァ
	public int setLendingParameter(RentalDTO dto) throws SQLException {
		String sql = "update rental_detail set lend_charge_id = ?, status_id = ? where id = ?";
		try(PreparedStatement ps = this.conn.prepareStatement(sql)) {
			ps.setString(1, dto.getLendChargeId());
			ps.setInt(2, dto.getStatusId());
			ps.setInt(3, dto.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	// 注文詳細貸出時にしか使わなそうな汎用性の無いメソッド。なんで俺がやらなあかんねんオラオラオラァ
		public int setReturnedParameter(RentalDTO dto) throws SQLException {
			String sql = "update rental_detail set return_date = ?, return_charge_id = ?, status_id = ? where id = ?";
			try(PreparedStatement ps = this.conn.prepareStatement(sql)) {
				ps.setDate(1, dto.getReturnDate());
				ps.setString(2, dto.getReturnChargeId());
				ps.setInt(3, dto.getStatusId());
				ps.setInt(4, dto.getId());
				return ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}
}