package jp.co.mmr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sun.prism.paint.RadialGradient;

import jp.co.mmr.dto.EmpDTO;

public class EmpDAO {

//	DBと連結するconnectionを定義する
	Connection conn;
	
//	他の処理から連結してきたconnを受け取って自身のconnectionに代入する
	public EmpDAO(Connection conn){
		this.conn = conn;
	}

	public boolean selectByIdAndPassword(String id, String password) throws SQLException {
//		selectByIdAndPasswordでユーザが存在するかどうかを検証する
		//LoginServletからidとpasswordを取得して？に代入する
		//DBでIDとPASSWORDが一致するcountの数を表示する 
		
		StringBuffer sb = new StringBuffer();
		sb.append("select");
		sb.append("      COUNT(*) 'COUNT'");
		sb.append(" from");
		sb.append("      EMPLOYEE");
		sb.append(" where");
		sb.append("      ID = ?");
		sb.append("   and PASSWORD = ?");
		sb.append("   and IS_DISPLAY = 1");
		
		try {
//			DBと連結するstatementを作る
			PreparedStatement ps = conn.prepareStatement(sb.toString());
			// id passwordが正しい場合１を返す　⇒　Trueになる
			ps.setString(1,  id);
			ps.setString(2,  password);
//			DBに問い合わせ、SQL文を表示する
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				return rs.getInt("COUNT") == 1;
			}
		} catch (SQLException e){
			e.printStackTrace();
			throw e;
		}
		return false;
	}
	
	public EmpDTO getEmpById(String id) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        employee");
        sb.append("    WHERE");
        sb.append("        id = ?");
        try (PreparedStatement ps = conn.prepareStatement(sb.toString())){
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                EmpDTO dto = new EmpDTO();
                dto.setEmpId(rs.getString("ID"));
                dto.setEmpName(rs.getString("NAME"));
                dto.setEmpPassword(rs.getString("PASSWORD"));
                dto.setEmpIsmaster(rs.getInt("IS_MASTER"));
                return dto;
            }
        } catch (SQLException e){
            e.printStackTrace();
            throw e;
        }
        return null;
	}
	
//	パスワード変更
	public boolean changepassword(String id, String newpassword)throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" UPDATE");
		sb.append("            employee");
		sb.append("        SET");
		sb.append("            password=?");
		sb.append("        WHERE");
		sb.append("            id=?");
		sb.append("   and IS_DISPLAY = 1");
		
		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){
			ps.setString(1, newpassword);
			ps.setString(2, id);
			int result = ps.executeUpdate();	
			
			if(result == 1){
				return true;
			}
			
			// 失敗なのでfalseを返す
			return false;
			
		}catch (SQLException e){
			e.printStackTrace();
			throw e;			
		}
	}

//	権限順で従業員テーブルのデータを全部表示する（テーブル内ではID順）　
	
	public ArrayList<EmpDTO> selectAll() throws SQLException{
		ArrayList<EmpDTO> empList = new ArrayList<>();
		
		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ID");
		sb.append("        ,NAME");
		sb.append("        ,IS_MASTER");
		sb.append("    FROM");
		sb.append("        EMPLOYEE");
		sb.append("   where IS_DISPLAY = 1");
		sb.append("    ORDER BY");
		sb.append("        is_master DESC");

		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){
			
			// SQLの実行
			ResultSet rs = ps.executeQuery();
			
			// データを一件ずつEmpDTOの形にしてリストへ追加
			while(rs.next()){
				EmpDTO emp = new EmpDTO();
				emp.setEmpId(rs.getString("ID"));
				emp.setEmpName(rs.getString("NAME"));
				emp.setEmpIsmaster(rs.getInt("IS_MASTER"));
				empList.add(emp);
			}
			// リストを返す
			return empList;
		} catch (SQLException e) {
			throw e;
		}
	}
	
//	empManager画面でパスワードリセット用
	
	public boolean resetPassword(String id)throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" UPDATE");
		sb.append("            employee");
		sb.append("        SET");
		sb.append("            password=default");
		sb.append("        WHERE");
		sb.append("            id=?");
		sb.append("   and IS_DISPLAY = 1");
		
		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){
			ps.setString(1, id);
			int result = ps.executeUpdate();	
			
			if(result == 1){
				return true;
			}
			// 失敗なのでfalseを返す
			return false;
			
		}catch (SQLException e){
			e.printStackTrace();
			throw e;			
		}
	}
	
	
//	従業員を追加
	
	public boolean addEmp(String id, String name, String is_master)throws SQLException{
		StringBuffer sb = new StringBuffer();

		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        employee(");
		sb.append("            id");
		sb.append("            ,name");
		sb.append("            ,is_master");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        )");

		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){
			ps.setString(1, id);
			ps.setString(2, name);
			ps.setString(3, is_master);
			
			int result = ps.executeUpdate();	
			
			if(result == 1){
				return true;
			}
			// 失敗なのでfalseを返す
			return false;
			
		}catch (SQLException e){
			e.printStackTrace();
			throw e;			
		}
	}
	

//	従業員データ修正
	public boolean empEdit(String id, String name, String is_master,String empId)throws SQLException{

	    StringBuffer sb = new StringBuffer();
	    sb.append("UPDATE");
	    sb.append("        employee");
	    sb.append("    SET");
	    sb.append("        id = ?");
	    sb.append("        ,name = ?");
	    sb.append("        ,is_master = ?");
	    sb.append("    WHERE");
	    sb.append("        id = ?");
	    sb.append("   and IS_DISPLAY = 1");

	    try(PreparedStatement ps = conn.prepareStatement(sb.toString())){

	        ps.setString(1, id);
	        ps.setString(2, name);
	        ps.setString(3, is_master);
	        ps.setString(4, empId);

	        int result = ps.executeUpdate();	

	        if(result == 1){
	            return true;
	        }
	        // 失敗なのでfalseを返す
	        return false;

	    }catch (SQLException e){
	        e.printStackTrace();
	        throw e;			
	    }
	}

//	従業員論理削除
	
	public boolean deleteEmp(String id)throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        employee");
		sb.append("    SET");
		sb.append("        is_display = 0");
		sb.append("    WHERE");
		sb.append("        id = ?");

		try(PreparedStatement ps = conn.prepareStatement(sb.toString())){
			ps.setString(1, id);
			int result = ps.executeUpdate();	
			
			if(result == 1){
				return true;
			}
			// 失敗なのでfalseを返す
			return false;
			
		}catch (SQLException e){
			e.printStackTrace();
			throw e;			
		}
	}
}
