package jp.co.mmr.dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.sun.xml.internal.bind.v2.model.core.ID;

import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.Validate;

public class MerchandiseDAO {

	private Connection con;

	public MerchandiseDAO(Connection con) {
		this.con = con;
	}
	
	/**
     * 蜈ｨ縺ｦ縺ｮ蝠�蜩√ｒ蜿門ｾ励☆繧�
     * @return
     * @throws SQLException
     */
    public ArrayList<MerchandiseDTO> getAllMerchandise() throws SQLException {
        StringBuffer sb = new StringBuffer();
         sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        merchandise");
        sb.append("    ORDER BY");
        sb.append("        id DESC");
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ResultSet rs = ps.executeQuery();
            ArrayList<MerchandiseDTO> list = new ArrayList<>();
            while(rs.next()) {
                MerchandiseDTO dto = new MerchandiseDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("name"));
                dto.setImage(rs.getBinaryStream("image"));
                dto.setCategoryid(rs.getInt("category_id"));
                dto.setJunleid(rs.getInt("junle_id"));
                dto.setArtistid(rs.getInt("artist_id"));
                dto.setPrice(rs.getInt("price"));
                dto.setArrivalid(rs.getInt("arrival_id"));
                dto.setOriginnum(rs.getInt("origin_num"));
                list.add(dto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * 讀懃ｴ｢譚｡莉ｶ(繧ｫ繝�繧ｴ繝ｪ縲√ず繝｣繝ｳ繝ｫ縲∝錐蜑�)縺ｫ荳�閾ｴ縺吶ｋ蜈ｨ縺ｦ縺ｮ蝠�蜩√ｒ蜿門ｾ励☆繧�
     * @return
     * @throws SQLException
     */
    public ArrayList<MerchandiseDTO> getMerchandiseBySearchParameter(Integer categoryId, Integer junleId, String name) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        merchandise");
        sb.append("    WHERE");
        sb.append("        '' = ''");
        
        if(categoryId != null){
            sb.append("        AND category_id = ?");
        }
        if (junleId != null) {
            sb.append("        AND junle_id = ?");
        }
        if (!(Validate.isEmpty(name))) {
            sb.append("        AND name LIKE ?");
        }
        sb.append("    ORDER BY");
        sb.append("        id DESC");

        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            int ps_index = 0;
            if(categoryId != null){
                ps_index++;
                ps.setInt(ps_index, categoryId);
            }
            if (junleId != null) {
                ps_index++;
                ps.setInt(ps_index, junleId);
            }
            if (!(Validate.isEmpty(name))) {
                ps_index++;
                ps.setString(ps_index, '%' + name + '%');
            }
            ResultSet rs = ps.executeQuery();
            ArrayList<MerchandiseDTO> list = new ArrayList<>();
            while(rs.next()) {
                MerchandiseDTO dto = new MerchandiseDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("name"));
                dto.setImage(rs.getBinaryStream("image"));
                dto.setCategoryid(rs.getInt("category_id"));
                dto.setJunleid(rs.getInt("junle_id"));
                dto.setArtistid(rs.getInt("artist_id"));
                dto.setPrice(rs.getInt("price"));
                dto.setArrivalid(rs.getInt("arrival_id"));
                dto.setOriginnum(rs.getInt("origin_num"));
                list.add(dto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }
	
	/**
	 * 蜿門ｾ励＠縺溘＞蝠�蜩√ｒ謖�螳壹＠縺ｦ蜿門ｾ励☆繧�
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public MerchandiseDTO getMerchandiseById(int id) throws SQLException {
	    StringBuffer sb = new StringBuffer();
	    sb.append("SELECT");
	    sb.append("        *");
	    sb.append("    FROM");
	    sb.append("        merchandise");
	    sb.append("    WHERE");
	    sb.append("        id = ?");
	    try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
	        ps.setInt(1, id);
	        ResultSet rs = ps.executeQuery();
	        if (rs.next()) {
	            MerchandiseDTO dto = new MerchandiseDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("name"));
                dto.setImage(rs.getBinaryStream("image"));
                dto.setCategoryid(rs.getInt("category_id"));
                dto.setJunleid(rs.getInt("junle_id"));
                dto.setArtistid(rs.getInt("artist_id"));
                dto.setPrice(rs.getInt("price"));
                dto.setArrivalid(rs.getInt("arrival_id"));
                dto.setOriginnum(rs.getInt("origin_num"));
                return dto;
            }
	    } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
	    return null;
	}
	
	/**
	 * 謖�螳壹＠縺溷膚蜩√�ｮ逕ｻ蜒上ョ繝ｼ繧ｿ縺ｮ縺ｿ繧貞叙蠕励☆繧�
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public BufferedImage getImageById(int id) throws IOException, SQLException {
	    MerchandiseDTO dto = getMerchandiseById(id);
	    InputStream is = dto.getImage();
	    BufferedInputStream bis = new BufferedInputStream(is);
	    return ImageIO.read(bis);
	}
		
	/**
	 * 蠑墓焚縺ｫ貂｡縺励◆MerchandiseDTO繧奪B縺ｫ逋ｻ骭ｲ縺吶ｋ
	 * id�ｼ�auto_increment�ｼ�, feature_id�ｼ亥膚蜩∬ｿｽ蜉�譎ゅ�ｯ縺ｪ縺�縺ｯ縺夲ｼ峨�ｯ逵∫払
	 * @param dto
	 * @return
	 * @throws SQLException
	 */
	public int setMerchandise(MerchandiseDTO dto) throws SQLException {
	    StringBuffer sb = new StringBuffer();
	    sb.append("INSERT");
	    sb.append("    INTO");
	    sb.append("        merchandise(");
	    sb.append("            name");
	    sb.append("            ,image");
	    sb.append("            ,category_id");
	    sb.append("            ,junle_id");
	    sb.append("            ,artist_id");
	    sb.append("            ,price");
	    sb.append("            ,arrival_id");
	    sb.append("            ,origin_num");
	    sb.append("        )");
	    sb.append("    VALUES");
	    sb.append("        (");
	    sb.append("            ?");
	    sb.append("            ,?");
	    sb.append("            ,?");
	    sb.append("            ,?");
	    sb.append("            ,?");
	    sb.append("            ,?");
	    sb.append("            ,?");
	    sb.append("            ,?");
	    sb.append("        )");
	    try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
	        ps.setString(1, dto.getName());
	        ps.setBlob(2, dto.getImage());
	        ps.setInt(3, dto.getCategoryid());
	        ps.setInt(4, dto.getJunleid());
	        ps.setInt(5, dto.getArtistid());
	        ps.setInt(6, dto.getPrice());
	        ps.setInt(7, dto.getArrivalid());
	        ps.setInt(8, dto.getOriginnum());
	        return ps.executeUpdate();
	    } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
	}

	// 陟大｢鍋�夂ｸｺ�ｽｧ陷ｿ蜉ｱ�ｿ�陷ｿ謔ｶ笆ｲ邵ｺ貅倥″郢晢ｿｽ郢ｧ�ｽｴ郢晢ｽｪ邵ｲ竏壹★郢晢ｽ｣郢晢ｽｳ郢晢ｽｫ邵ｲ竏晁�夊惓竏晞倹邵ｺ�ｽｧ隶�諛�ｽｴ�ｽ｢郢ｧ螳夲ｽ｡蠕娯鴬郢晢ｽ｡郢ｧ�ｽｽ郢晢ｿｽ郢晢ｿｽ
	public ArrayList<MerchandiseDTO> searchItem(int cateNo, int junNo, String itemname) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		// SQL隴�ｿｽ邵ｺ�ｽｮ闖ｴ諛茨ｿｽ�ｿｽ
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        category_id = ?");
		sb.append("        AND junle_id = ?");
		sb.append("        AND name LIKE ?");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setInt(1, cateNo);
			ps.setInt(2, junNo);
			ps.setString(3, itemname + "%");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public ArrayList<MerchandiseDTO> searchName(String itemname) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        name LIKE ?");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {

			ps.setString(1, itemname + "%");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public ArrayList<MerchandiseDTO> cateJunNo(int cateNo, int junNo) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        category_id = ?");
		sb.append("        AND junle_id = ?");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setInt(1, cateNo);
			ps.setInt(2, junNo);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public ArrayList<MerchandiseDTO> cateNo(int cateNo) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        category_id = ?;");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setInt(1, cateNo);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}
	
    public int getNewId() throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        MAX(id)");
        sb.append("    FROM");
        sb.append("        merchandise limit 1");
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("MAX(ID)") + 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return 1;
    }
}