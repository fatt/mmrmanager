package jp.co.mmr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.mmr.dto.PurchaseDTO;

public class PurchaseDAO {
	
	private Connection con;

	public PurchaseDAO(Connection con) {
		this.con = con;
	}
	
	public ArrayList<PurchaseDTO> getPurchaseByRentalId(int id) throws SQLException {
		// DBViewerないからプレーン by自宅
		String sql = "select * from purchase_detail where rental_id = ?";
		try(PreparedStatement ps = this.con.prepareStatement(sql)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			ArrayList<PurchaseDTO> list = new ArrayList<>();
			while(rs.next()) {
				PurchaseDTO dto = new PurchaseDTO();
				dto.setId(rs.getInt("ID"));
				dto.setRentalId(rs.getInt("RENTAL_ID"));
				dto.setMerchandiseId(rs.getInt("MERCHANDISE_ID"));
				dto.setStatusId(rs.getInt("STATUS_ID"));
				dto.setOriginId(rs.getInt("ORIGIN_NUM"));
				dto.setReturnChargeId(rs.getString("RETURN_CHARGE_ID"));
				list.add(dto);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	// 注文詳細貸出時にしか使わなそうな汎用性の無いメソッド。SQL書くのめんどい、眠い、許して
	public int setLendingParameter(PurchaseDTO dto) throws SQLException {
	    StringBuffer sb = new StringBuffer();
	    sb.append("UPDATE");
	    sb.append("        purchase_detail");
	    sb.append("    SET");
	    sb.append("        origin_num = ?");
	    sb.append("        ,return_charge_id = ?");
	    sb.append("        ,status_id = ?");
	    sb.append("    WHERE");
	    sb.append("        id = ?");
		try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
		    ps.setInt(1, dto.getOriginId());
			ps.setString(2, dto.getReturnChargeId());
			ps.setInt(3, dto.getStatusId());
			ps.setInt(4, dto.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	// 注文詳細貸出時にしか使わなそうな汎用性の無いメソッド。SQL書くのめんどい、眠い、許して
	public int setReturnedParameter(PurchaseDTO dto) throws SQLException {
		String sql = "update purchase_detail set return_charge_id = ?, status_id = ? where id = ?";
		try(PreparedStatement ps = this.con.prepareStatement(sql)) {
		    ps.setString(1, dto.getReturnChargeId());
			ps.setInt(2, dto.getStatusId());
			ps.setInt(3, dto.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
