package jp.co.mmr.dto;

public class EmpDTO {

	private String empId;
	private String empPassword;
	private String empName;
	private int empIsmaster;
	private int empIsdisplay;
	private String editEmpId;
	private String editEmpName;
	private String editEmpIsmater;
	private String addEmpId;
	private String addEmpName;
	private String addEmpIsmaster;
	


	
	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}
	
	public String getEmpPassword(){
		return empPassword;
	}
	public void setEmpPassword(String empPassword){
		this.empPassword = empPassword;
	}
	
	public String getEmpName(){
		return empName;
	}
	
	public void setEmpName(String empName){
		this.empName = empName;
	}
	
	public int getEmpIsmaster(){
		return empIsmaster;
	}
	
	public void setEmpIsmaster(int empIsmaster){
		this.empIsmaster = empIsmaster;
	}
	public int getEmpIsdisplay(){
		return empIsdisplay;
	}
	public void setEmpIsdisplay(int empIsdisplay){
		this.empIsdisplay = empIsdisplay;
	}

//	�]�ƈ���ʕҏW�p
	
	public String getEditEmpId(){
		return editEmpId;
	}
	public void setEditEmpId(String editEmpId){
		this.editEmpId = editEmpId;
	}
	public String getEditEmpName(){
		return editEmpName;
	}
	public void setEditEmpName(String editEmpName){
		this.editEmpName = editEmpName;
	}
	public String getEditEmpIsmaster(){
		return editEmpIsmater;
	}
	public void setEditEmpIsmaster(String editEmpIsmater){
		this.editEmpIsmater = editEmpIsmater;
	}
	
//	�]�ƈ��ǉ��p
	
	public void setAddEmp(String addempid,String addempname,String addempismaster){
		this.addEmpId = addempid;
		this.addEmpName = addempname;
		this.addEmpIsmaster = addempismaster;
	}
	public String getAddEmpId(){
		return addEmpId;
	}
	public String getAddEmpName(){
		return addEmpName;
	}	
	public String getAddEmpIsmaster(){
		return addEmpIsmaster;
	}
	
}
