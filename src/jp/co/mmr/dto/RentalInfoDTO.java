package jp.co.mmr.dto;

import java.sql.Date;

public class RentalInfoDTO {
	private int rentalId;
	private int userId;
	private Date receiptDate;
	private Date returnDeadline;
	private Date returnDate;
	private String status;
	private int merchandiseId;
	private String title;
	private String arrival;
	private String artist;
	private String category;
	private String junle;
	private int price;
	private String lendCharge;
	private String returnCharge;
	private int originNum;
	public int getRentalId() {
		return rentalId;
	}
	public void setRentalId(int rentalId) {
		this.rentalId = rentalId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	public Date getReturnDeadline() {
		return returnDeadline;
	}
	public void setReturnDeadline(Date returnDeadline) {
		this.returnDeadline = returnDeadline;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getMerchandiseId() {
		return merchandiseId;
	}
	public void setMerchandiseId(int merchandiseId) {
		this.merchandiseId = merchandiseId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArrival() {
		return arrival;
	}
	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getJunle() {
		return junle;
	}
	public void setJunle(String junle) {
		this.junle = junle;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getLendCharge() {
		return lendCharge;
	}
	public void setLendCharge(String lendCharge) {
		this.lendCharge = lendCharge;
	}
	public String getReturnCharge() {
		return returnCharge;
	}
	public void setReturnCharge(String returnCharge) {
		this.returnCharge = returnCharge;
	}
	public int getOriginNum() {
		return originNum;
	}
	public void setOriginNum(int originNum) {
		this.originNum = originNum;
	}

}
