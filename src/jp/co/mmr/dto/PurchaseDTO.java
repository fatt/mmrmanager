package jp.co.mmr.dto;

public class PurchaseDTO {
	//�A�N�Z�T���\�b�h�p�ϐ���`
	private int id;
	private int rentalId;
	private int merchandiseId;
	private int statusId;
	private int originId;
	private String returnChargeId;
	

	//�w��ID
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	//�����^��ID
	public int getRentalId() {
		return rentalId;
	}
	public void setRentalId(int rentalId) {
		this.rentalId = rentalId;
	}
	
	//���iID
	public int getMerchandiseId() {
		return merchandiseId;
	}
	public void setMerchandiseId(int merchandiseId) {
		this.merchandiseId = merchandiseId;
	}
	
	//�X�e�[�^�X
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	
	//�̎��ʔԍ�
	public int getOriginId() {
		return originId;
	}
	public void setOriginId(int originId) {
		this.originId = originId;
	}

	public String getReturnChargeId() {
		return returnChargeId;
	}
	public void setReturnChargeId(String returnChargeId) {
		this.returnChargeId = returnChargeId;
	}
}
