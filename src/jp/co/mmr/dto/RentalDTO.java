package jp.co.mmr.dto;

import java.sql.Date;

public class RentalDTO {
	//�A�N�Z�T���\�b�h�p�ϐ���`
	private int id;
	private int userId;
	private Date receiptDate;
	private Date deliveryDate;
	private int delieryTime;
	private Date returnDeadline;
	private Date returnDate;
	private int orderQuantity;
	private String lendChargeId;
	private String returnChargeId;
	private int statusId;
	
	
	//���͂��\���
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	//���͂��\�莞��
	public int getDelieryTime() {
		return delieryTime;
	}
	
	public void setDelieryTime(int delieryTime) {
		this.delieryTime = delieryTime;
	}

	public int getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(int orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	//�����ԍ�
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	//���[�UID
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	//������t����
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	
	//�ԋp�\���
	public Date getReturnDeadline() {
		return returnDeadline;
	}
	public void setReturnDeadline(Date returnDeadline) {
		this.returnDeadline = returnDeadline;
	}
	
	//�ԋp��
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	
	//�ݏo�S����ID
	public String getLendChargeId() {
		return lendChargeId;
	}
	public void setLendChargeId(String lendChargeId) {
		this.lendChargeId = lendChargeId;
	}
	
	//�ԋp�S����ID
	public String getReturnChargeId() {
		return returnChargeId;
	}
	public void setReturnChargeId(String returnChargeId) {
		this.returnChargeId = returnChargeId;
	}
	
	//�X�e�[�^�X�i��t�E�ݏo�E�ؔ[�E�ԋp�j
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
}
