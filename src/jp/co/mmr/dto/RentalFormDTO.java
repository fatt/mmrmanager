package jp.co.mmr.dto;
import java.sql.SQLException;

import javax.naming.NamingException;

import jp.co.mmr.util.Converter;

public class RentalFormDTO {

	private String id;
	private String userId;
	private String recipetDate;
	private String delivaryDate;
	private String  returnDeadline;
	private String returnDate;
	private String orderQuantity;
	private String lendChargeId;
	private String returnChargeId;
	private String statusId;		
	
	public RentalFormDTO() {}

	public RentalFormDTO(RentalDTO dto) throws SQLException, NamingException {
		this.id = String.valueOf(dto.getId());
		this.userId = String.valueOf(dto.getUserId());
		this.recipetDate = String.valueOf(dto.getReceiptDate());
		this.delivaryDate = String.valueOf(dto.getDeliveryDate());
		this.returnDeadline = String.valueOf(dto.getReturnDeadline());
		this.returnDate = String.valueOf(dto.getReturnDate());
		this.orderQuantity = String.valueOf(dto.getOrderQuantity());
		this.lendChargeId = String.valueOf(dto.getLendChargeId());
		this.returnChargeId = String.valueOf(dto.getReturnChargeId());
		this.statusId = Converter.getStatusByStatusId(dto.getStatusId());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRecipetDate() {
		return recipetDate;
	}

	public void setRecipetDate(String recipetDate) {
		this.recipetDate = recipetDate;
	}

	public String getDelivaryDate() {
		return delivaryDate;
	}

	public void setDelivaryDate(String delivaryDate) {
		this.delivaryDate = delivaryDate;
	}

	public String getReturnDeadline() {
		return returnDeadline;
	}

	public void setReturnDeadline(String returnDeadline) {
		this.returnDeadline = returnDeadline;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(String orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public String getLendChargeId() {
		return lendChargeId;
	}

	public void setLendChargeId(String lendChargeId) {
		this.lendChargeId = lendChargeId;
	}

	public String getReturnChargeId() {
		return returnChargeId;
	}

	public void setReturnChargeId(String returnChargeId) {
		this.returnChargeId = returnChargeId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	
	
	


}
