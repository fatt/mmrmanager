package jp.co.mmr.dto;

import java.sql.Date;

public class UserDTO {
	//アクセサメソッド用変数定義
	private int id;
	private String userName;
	private String userPassword;
	private String mailAddress;
	private int postNum;
	private String address;
	private String tell;
	private Date birthday;
	private String creditNum;
	private String creditOwnerName;
	private int expirationDate;
	private String securityCode;

	//ID
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	//ユーザ名
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	//パスワード
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	//メールアドレス
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	//郵便番号
	public int getPostNum() {
		return postNum;
	}
	public void setPostNum(int postNum) {
		this.postNum = postNum;
	}
	
	//住所
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	//電話番号
	public String getTell() {
		return tell;
	}
	public void setTell(String tell) {
		this.tell = tell;
	}
	
	
	//生年月日
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	//クレジットカード番号
	public String getCreditNum() {
		return creditNum;
	}
	public void setCreditNum(String creditNum) {
		this.creditNum = creditNum;
	}
	
	//カード名義人
	public String getCreditOwnerName() {
		return creditOwnerName;
	}
	public void setCreditOwnerName(String creditOwnerName) {
		this.creditOwnerName = creditOwnerName;
	}
	//有効期限
	public int getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(int expirationDate) {
		this.expirationDate = expirationDate;
	}
	//セキュリティコード
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
}
