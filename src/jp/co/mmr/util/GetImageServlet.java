package jp.co.mmr.util;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.FeatureDAO;
import jp.co.mmr.dao.MerchandiseDAO;

@WebServlet(name = "getImage", urlPatterns = { "/getImage" })
public class GetImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    // 画像のIDを取得する
	    Integer featureId = Validate.convertStrToInteger(request.getParameter("feature_id"));
	    Integer merchandiseId = Validate.convertStrToInteger(request.getParameter("merchandise_id"));
	    OutputStream os = response.getOutputStream();
	    try(Connection con = DataSourceManager.getConnection()) {
	        if (featureId != null) {
	            // 取得したIDにひもづく画像を取得
	            FeatureDAO featureDao = new FeatureDAO(con);
	            BufferedImage img = featureDao.getImageById(featureId);
	            // 取得した画像をクライアントに返却
	            response.setContentType("image/jpeg");
	            ImageIO.write(img, "jpg", os);
	        } else if (merchandiseId != null) {
	            // 取得したIDにひもづく画像を取得
                MerchandiseDAO merchandiseDao = new MerchandiseDAO(con);
                BufferedImage img = merchandiseDao.getImageById(merchandiseId);
                // 取得した画像をクライアントに返却
                response.setContentType("image/jpeg");
                ImageIO.write(img, "jpg", os);
	        }

        } catch (IOException | SQLException | NamingException e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        } finally {
            os.flush();
        }
	}

}