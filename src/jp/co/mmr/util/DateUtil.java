package jp.co.mmr.util;

import java.sql.Date;
import java.util.Calendar;

public class DateUtil {

    public static Date addSQLDate(java.util.Date date, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, amount);
        Date addedDate = new Date(cal.getTime().getTime());
        return addedDate;
    }
    
}
