package jp.co.mmr.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;

@WebServlet("/getMerchandise")
public class GetMerchandiseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    resp.setContentType("application/json; charset=utf-8");

	    Integer categoryId = Validate.convertStrToInteger(Validate.getStrByObject(req.getParameter("categoryId")));
        Integer junleId = Validate.convertStrToInteger(Validate.getStrByObject(req.getParameter("junleId")));
        String name = Validate.getStrByObject(req.getParameter("name"));
	    
	    PrintWriter pw = resp.getWriter();
	    try(Connection con = DataSourceManager.getConnection()) {
	    	MerchandiseDAO merchandiseDao = new MerchandiseDAO(con);
	        ArrayList<MerchandiseDTO> merchandiseList = merchandiseDao.getMerchandiseBySearchParameter(
                    categoryId,
	                junleId,
	                name
	                );
	        pw.println("{\"merchandiseList\" : [");
	        for(int i = 0; i < merchandiseList.size(); i++) {
                pw.print("{");
                pw.print("\"line\" : \"" + (i+1) + "\",");
                pw.print("\"id\" : \"" + merchandiseList.get(i).getId() + "\",");
                pw.print("\"name\" : \"" + merchandiseList.get(i).getName() + "\",");
                pw.print("\"arrival\" : \"" + Converter.getArrivalByArrivalId(merchandiseList.get(i).getArrivalid()) + "\",");
                pw.print("\"category\" : \"" + Converter.getCategoryByCategoryId(merchandiseList.get(i).getCategoryid()) + "\",");
                pw.print("\"junle\" : \"" + Converter.getJunleByJunleId(merchandiseList.get(i).getJunleid()) + "\",");
                pw.print("\"artist\" : \"" + Converter.getArtistByArtistId(merchandiseList.get(i).getArtistid()) + "\",");
                pw.print("\"price\" : \"" + merchandiseList.get(i).getPrice() + "\"");
                pw.print("}");
	            if (i != merchandiseList.size() - 1) {
					pw.print(",");
				}
	        }
	        pw.println("],");
	        pw.print("\"listSize\" : \"" + merchandiseList.size() + "\",");
	        pw.print("\"searchCategory\" : \"" + Converter.getCategoryByCategoryId(categoryId != null ? categoryId : 0) + "\",");
	        pw.print("\"searchJunle\" : \"" + Converter.getJunleByJunleId(junleId != null ? junleId : 0) + "\",");
	        pw.print("\"searchName\" : \"" + name + "\"");
	        pw.println("}");
	    } catch (SQLException | NamingException e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }
	    
	}

}
