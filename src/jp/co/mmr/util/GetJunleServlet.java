package jp.co.mmr.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dto.JunleDTO;

@WebServlet("/getJunle")
public class GetJunleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String REQUEST_STRING = "categoryId";
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    resp.setContentType("application/json; charset=utf-8");
	    String categoryId = req.getParameter(REQUEST_STRING);
	    PrintWriter pw = resp.getWriter();
	    try {
	        ArrayList<JunleDTO> junleList = Converter.getJunleListByCategoryId(Validate.convertStrToInteger(categoryId));
	        pw.println("{\"junleList\" : [");
	        for(int i = 0; i < junleList.size(); i++) {
	            if (i == junleList.size() -1 ) {
	                pw.print("{");
	                pw.print("\"id\" : \"" + junleList.get(i).getId() + "\",");
	                pw.print("\"name\" : \"" + junleList.get(i).getName() + "\"");
	                pw.print("}");
	                
	            } else {
	                pw.print("{");
                    pw.print("\"id\" : \"" + junleList.get(i).getId() + "\",");
                    pw.print("\"name\" : \"" + junleList.get(i).getName() + "\"");
                    pw.print("},");
	            }
	        }
	        pw.println("]}");
	    } catch (SQLException | NamingException e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }
	}
}
