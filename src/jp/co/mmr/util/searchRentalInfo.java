package jp.co.mmr.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.RentalDTO;


@WebServlet("/searchRentalInfo")
public class searchRentalInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("application/json; charset=utf-8");
		 
		 Date receiptDate =  Date.valueOf(Validate.getStrByObject(request.getParameter("receiptDate")));
		 Date returnDeadline =  Date.valueOf(Validate.getStrByObject(request.getParameter("returnDeadline")));
		 Date returnDate =  Date.valueOf(Validate.getStrByObject(request.getParameter("returnDate")));
		 String lendCharge = Validate.getStrByObject(request.getParameter("lendCharge"));
		 String returnCharge = Validate.getStrByObject(request.getParameter("returnCharge"));
		 int status = Integer.valueOf(Validate.getStrByObject(request.getParameter("status")));
		 
		 PrintWriter pw = response.getWriter();
		 try(Connection con = DataSourceManager.getConnection()){
			 RentalDAO dao = new RentalDAO(con);
			 ArrayList<RentalDTO> rentalList = dao.getSortRentalInfo(
					 receiptDate,
					 returnDeadline,
					 returnDate,
					 lendCharge,
					 returnCharge,
					 status
					 );
		        pw.println("{\"rentalList\" : [");
		        for(int i = 0; i < rentalList.size(); i++) {
	                pw.print("\"receiptDate\" : \"" + rentalList.get(i).getReceiptDate() + "\"");
	                pw.print("\"returnDeadline\" : \"" + rentalList.get(i).getReturnDeadline() + "\"");
	                pw.print("\"returnDate\" : \"" + rentalList.get(i).getReturnDate() + "\"");
	                pw.print("\"lendCharge\" : \"" + rentalList.get(i).getLendChargeId() + "\"");
	                pw.print("\"returnCharge\" : \"" + rentalList.get(i).getReturnChargeId() + "\"");
	                pw.print("\"status\" : \"" + rentalList.get(i).getStatusId() + "\"");
	                pw.print("}");
		            if (i != rentalList.size() - 1) {
						pw.print(",");
					}
		        }
		        pw.println("],");
		        pw.print("\"listSize\" : \"" + rentalList.size() + "\",");
		        pw.print("\"searchReceiptDate\" : \"" + receiptDate + "\"");
		        pw.print("\"searchReturnDeadline\" : \"" + returnDeadline + "\"");
		        pw.print("\"searchReturnDate\" : \"" + returnDate + "\"");
		        pw.print("\"searchLendCharge\" : \"" + lendCharge + "\"");
		        pw.print("\"searchReturnCharge\" : \"" + returnCharge + "\"");
		        pw.print("\"searchStatus\" : \"" + status + "\"");
		        pw.println("}");
		    } catch (SQLException | NamingException e) {
	            e.printStackTrace();
	            response.sendRedirect("error.jsp");
	        }
		    
		}

	}