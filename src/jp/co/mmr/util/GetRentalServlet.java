package jp.co.mmr.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.RentalDTO;


@WebServlet("/getRental")
public class GetRentalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("application/json; charset=utf-8");
		 
		 String receiptDateStr = Validate.getStrByObject(request.getParameter("receiptDate"));
         Date receiptDate = null;
         if (!(Validate.isEmpty(receiptDateStr))) {
             receiptDate = Date.valueOf(receiptDateStr);
         }
         
         String returnDeadlineStr = Validate.getStrByObject(request.getParameter("returnDeadline"));
         Date returnDeadline = null;
         if (!(Validate.isEmpty(returnDeadlineStr))) {
             returnDeadline = Date.valueOf(returnDeadlineStr);
         }
         
         String returnDateStr = Validate.getStrByObject(request.getParameter("returnDate"));
         Date returnDate = null;
         if (!(Validate.isEmpty(returnDateStr))) {
             returnDate = Date.valueOf(returnDateStr);
         }
         
		 String lendCharge = Validate.getStrByObject(request.getParameter("lendCharge"));
		 String returnCharge = Validate.getStrByObject(request.getParameter("returnCharge"));
		 
		 String statusStr = Validate.getStrByObject(request.getParameter("status"));
		 int status = 0;
		 if (!(Validate.isEmpty(statusStr))) {
		     status = Validate.convertStrToInteger(statusStr);
        }
		 
		 PrintWriter pw = response.getWriter();
		 try(Connection con = DataSourceManager.getConnection()){
		     RentalDAO dao = new RentalDAO(con);
		     ArrayList<RentalDTO> rentalList = dao.getSortRentalInfo(
		             receiptDate,
		             returnDeadline,
		             returnDate,
		             lendCharge,
		             returnCharge,
		             status
		             );
		     pw.println("{\"rentalList\" : [");
		     for(int i = 0; i < rentalList.size(); i++) {
		         pw.print("{");
		         pw.print("\"id\" : \"" + rentalList.get(i).getId() + "\",");
		         pw.print("\"receiptDate\" : \"" + rentalList.get(i).getReceiptDate() + "\",");
		         pw.print("\"returnDeadLine\" : \"" + rentalList.get(i).getReturnDeadline() + "\",");
		         pw.print("\"returnDate\" : \"" + (rentalList.get(i).getReturnDate() != null ? rentalList.get(i).getReturnDate() : "") + "\",");
		         pw.print("\"orderQuantity\" : \"" + rentalList.get(i).getOrderQuantity() + "\",");
		         pw.print("\"lendCharge\" : \"" + (rentalList.get(i).getLendChargeId() != null ? rentalList.get(i).getLendChargeId() : "") + "\",");
		         pw.print("\"returnCharge\" : \"" + (rentalList.get(i).getReturnChargeId() != null ? rentalList.get(i).getReturnChargeId() : "") + "\",");
		         pw.print("\"status\" : \"" + Converter.getStatusByStatusId(rentalList.get(i).getStatusId()) + "\"");
		         pw.print("}");
		         if (i != rentalList.size() - 1) {
		             pw.print(",");
		         }
		     }
		     pw.println("]}");
		 } catch (SQLException | NamingException e) {
	            e.printStackTrace();
	            response.sendRedirect("error.jsp");
	     }
	}
}
