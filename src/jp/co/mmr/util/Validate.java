package jp.co.mmr.util;

import java.util.regex.Pattern;

/**
 * 入力チェック用クラス
 * @author RyosukeOmori
 */
public abstract class Validate {
    
    /**
     * 与えられたString値が空又はnullの場合にtrueを返す
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str == null || str.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 与えられたString値が数値のみの場合にtrueを返す
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
        return Pattern.compile("\\d+").matcher(str).matches();
    }
    
    /**
     * 与えられたString値が1byte文字(半角英数字)のみの場合にtrueを返す
     * @param str
     * @return
     */
    public static boolean isHankaku(String str) {
        return Pattern.compile("^[0-9a-zA-Z]+$").matcher(str).matches();
    }
    
    /**
     * isHankaku + 与えられたint値以上の文字数である場合にtrueを返す
     * @param str
     * @param len
     * @return
     */
    public boolean isHankakuWithLength(String str, int len) {
        return (Validate.isHankaku(str) && str.length() >= len);
    }

    /**
     * 入力チェックも行うInteger.parseInt()
     * @param str
     * @return
     */
    public static Integer convertStrToInteger(String str) {
        if (!(Validate.isEmpty(str)) && Validate.isNumber(str)) {
            return Integer.parseInt(str);
        } else {
            return null;
        }
    }
    
    public static String getStrByObject(Object value) {
        if (value != null) {
            String str = String.valueOf(value);
            if (!(Validate.isEmpty(str))) {
                return str;
            }
        }
        return null;
    }

}