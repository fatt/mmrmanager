package jp.co.mmr.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;

import jp.co.mmr.dto.JunleDTO;

/**
 * ���̓`�F�b�N�p�N���X
 * @author RyosukeOmori
 */
public abstract class Converter {
    
    /**
     * �X�e�[�^�XID�ɊY�����镶������擾����
     * @param id
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public static String getStatusByStatusId(int id) throws SQLException, NamingException {
        String sql = "select name from status where id = ?";
        try(Connection con = DataSourceManager.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("NAME");
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
    /**
     * �J�e�S��ID�ɊY�����镶������擾����
     * @param id
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public static String getCategoryByCategoryId(int id) throws SQLException, NamingException {
        String sql = "select name from category where id = ?";
        try(Connection con = DataSourceManager.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("NAME");
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
    /**
     * �W������ID�ɊY�����镶������擾����
     * @param id
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public static String getJunleByJunleId(int id) throws SQLException, NamingException {
        String sql = "select name from junle where id = ?";
        try(Connection con = DataSourceManager.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("NAME");
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
    /**
     * �J�e�S��ID�ɊY������W���������X�g���擾����
     * @param id
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public static ArrayList<JunleDTO> getJunleListByCategoryId(int id) throws SQLException, NamingException {
        String sql = "select * from junle where category_id = ?";
        try(Connection con = DataSourceManager.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            ArrayList<JunleDTO> junleList = new ArrayList<>();
            while (rs.next()) {
                JunleDTO dto = new JunleDTO();
                dto.setId(rs.getInt("ID"));
                dto.setName(rs.getString("NAME"));
                dto.setCategoryId(rs.getInt("CATEGORY_ID"));
                junleList.add(dto);
            }
            return junleList;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * �V���敪ID�ɊY�����镶������擾����
     * @param id
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public static String getArrivalByArrivalId(int id) throws SQLException, NamingException {
        String sql = "select name from arrival where id = ?";
        try(Connection con = DataSourceManager.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("NAME");
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
    /**
     * �A�[�e�B�X�gID�ɊY�����镶������擾����
     * @param id
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public static String getArtistByArtistId(int id) throws SQLException, NamingException {
        String sql = "select name from artist where id = ?";
        try(Connection con = DataSourceManager.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("NAME");
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
    /**
     * �z������ID�ɊY�����镶������擾����
     * @param id
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    public static String getDeliveryTimeByDeliveryTimeId(int id) throws SQLException, NamingException {
        String sql = "select name from delivery_time where id = ?";
        try(Connection con = DataSourceManager.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("NAME");
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
}