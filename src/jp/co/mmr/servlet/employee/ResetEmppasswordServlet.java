package jp.co.mmr.servlet.employee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.DataSourceManager;
import jp.co.mmr.dao.EmpDAO;


@WebServlet("/ResetEmppassword")
public class ResetEmppasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

//	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
//			throws ServletException, IOException {
////		URLで連結された場合、ログイン画面に転送する
//		response.sendRedirect("login.jsp");
//
//		String resetEmpId = request.getParameter("empId");
//		if(resetEmpId == null){
//			// IDがない場合、PreEmpManagerServletに転送する
//			request.getRequestDispatcher("/preEmpManager").forward(request, response);
//			return;
//		}
//		
//		HttpSession session = request.getSession();
//		
//		try(Connection conn = DataSourceManager.getConnection()){
//			EmpDAO dao = new EmpDAO(conn);
//			
////			DAOを通してパスワードをリセットする処理
//			if(dao.resetPassword(resetEmpId)){
//				session.setAttribute("resetpassword_message", "パスワードリセットが成功しました");
//			}else{
//				session.setAttribute("resetpassword_message", "パスワードリセットが失敗しました");
//				request.getRequestDispatcher("/preEmpManager").forward(request, response);
//			}
//			
////			業員管理一覧画面に戻す
//			request.getRequestDispatcher("/preEmpManager").forward(request, response);
//			return;
//		}catch (SQLException | NamingException e) {
//			e.printStackTrace();
//			response.sendRedirect("error.jsp");
//			return;
//		}
//	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リセットする従業員番号を取得
		String resetEmpId = request.getParameter("empId");
		if(resetEmpId == null){
			// IDがない場合、PreEmpManagerServletに転送する
			request.getRequestDispatcher("/preEmpManager").forward(request, response);
		}
		
		HttpSession session = request.getSession();
		
		try(Connection conn = DataSourceManager.getConnection()){
			EmpDAO dao = new EmpDAO(conn);
			
//			DAOを通してパスワードをリセットする処理
			if(dao.resetPassword(resetEmpId)){
				session.setAttribute("resetpassword_message", "パスワードリセットが成功しました");
			}else{
				session.setAttribute("resetpassword_message", "パスワードリセットが失敗しました");
				request.getRequestDispatcher("/preEmpManager").forward(request, response);
			}
			
//			業員管理一覧画面に戻す
			request.getRequestDispatcher("/preEmpManager").forward(request, response);
		}catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}

	}
}
