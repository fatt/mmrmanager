package jp.co.mmr.servlet.employee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.DataSourceManager;
import jp.co.mmr.dao.EmpDAO;


@WebServlet("/deleteEmp")
public class DeleteEmpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 削除する従業員番号を取得
		String delectEmpId = request.getParameter("empId");
		if(delectEmpId == null){
			// IDがない場合、PreEmpManagerServletに転送する
			request.getRequestDispatcher("/preEmpManager").forward(request, response);
		}
		HttpSession session = request.getSession(false);
		
		try(Connection conn = DataSourceManager.getConnection()){
			EmpDAO dao = new EmpDAO(conn);
			
//			DAOを通して論理削除する
			
			if(dao.deleteEmp(delectEmpId)){
				session.setAttribute("delete_message", "従業員削除が成功しました");
			}else{
				session.setAttribute("delete_message", "従業員削除が失敗しました");
			}
			
//			業員管理一覧画面に戻す
			request.getRequestDispatcher("/preEmpManager").forward(request, response);
		}catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}

	}
}
