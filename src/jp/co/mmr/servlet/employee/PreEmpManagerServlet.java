package jp.co.mmr.servlet.employee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.DataSourceManager;
import jp.co.mmr.dao.EmpDAO;
import jp.co.mmr.dto.EmpDTO;


@WebServlet("/preEmpManager")
public class PreEmpManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		// ログイン画面へ
		res.sendRedirect("login.jsp");
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		// EMP表の全データを表示する
		ArrayList<EmpDTO> list = new ArrayList<>();
		
		try(Connection conn = DataSourceManager.getConnection()){
			EmpDAO dao = new EmpDAO(conn);
			list = dao.selectAll();
			
			// セッションに結果を保存
			req.getSession().setAttribute("empList", list);
			
			// empManager.jspへ転送
			req.getRequestDispatcher("/WEB-INF/empManager.jsp").forward(req, res);

		}catch(SQLException | NamingException e){
			// エラーページへ転送
			req.getRequestDispatcher("/error.jsp").forward(req, res);
		}
	}
}
