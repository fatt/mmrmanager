package jp.co.mmr.servlet.employee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.DataSourceManager;
import jp.co.mmr.dao.EmpDAO;
import jp.co.mmr.dto.EmpDTO;

/**
 * Servlet implementation class ComfirmEmpServlet
 */
@WebServlet("/EditEmp")
public class EditEmpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
//		URLで連結された場合、ログイン画面に転送する
		response.sendRedirect("login.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
//		従業員画面から入力された編集データを取得する
		request.setCharacterEncoding("UTF-8");
		String editEmpId = request.getParameter("editEmpId");
		String editEmpName = request.getParameter("editEmpName");
		String editIsmaster = request.getParameter("editIsmaster");
//		編集する対象のIDを取得する
		String empId = request.getParameter("empId");
		
		HttpSession session = request.getSession();
		
//		DBとのconnectionをとる
		
		try(Connection conn = DataSourceManager.getConnection()) {
			
//			DAOと連結してconnectionを代入する
			EmpDAO dao = new EmpDAO(conn);
			
//			EmpDAOに送信する。EmpDAOを用いてデータベースと連結して従業員データを更新する
			EmpDTO dto = new EmpDTO();
			dto.setEditEmpId(editEmpId);
			dto.setEditEmpName(editEmpName);
			dto.setEditEmpIsmaster(editIsmaster);
			
			if(dao.empEdit(dto.getEditEmpId(),dto.getEditEmpName(),dto.getEditEmpIsmaster(),empId)){
//				データ更新成功
				request.getRequestDispatcher("preEmpManager").forward(request, response);
				session.setAttribute("edit_message", "従業員データ更新が成功しました");
			}else{
//				データ更新失敗
				request.getRequestDispatcher("preEmpManager").forward(request, response);
				session.setAttribute("edit_message", "従業員データ更新が失敗しました");
			}
		}catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}

	}
}
