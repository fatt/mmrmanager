package jp.co.mmr.servlet.employee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.DataSourceManager;
import jp.co.mmr.dao.EmpDAO;
import jp.co.mmr.dto.EmpDTO;

/**
 * Servlet implementation class AddEmpServlet
 */
@WebServlet("/addEmp")
public class AddEmpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
//		URL�ŘA�����ꂽ�ꍇ�A���O�C����ʂɓ]������
		response.sendRedirect("login.jsp");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
//		�]�ƈ���ʂ�����͂��ꂽ�ҏW�f�[�^���擾����
		request.setCharacterEncoding("UTF-8");
		String addEmpId = request.getParameter("addEmpId");
		String addEmpName = request.getParameter("addEmpName");
		String addIsmaster = request.getParameter("addIsmaster");
		
		HttpSession session = request.getSession();
		
//		DB�Ƃ�connection���Ƃ�
		try(Connection conn = DataSourceManager.getConnection()) {
			
//			DAO�ƘA������connection��������
			EmpDAO dao = new EmpDAO(conn);
			
			dao.addEmp(addEmpId, addEmpName, addIsmaster);
			
			

			
//			if(dao.empEdit(dto.getEditEmpId(),dto.getEditEmpName(),dto.getEditEmpIsmaster(),empId)){
////				�f�[�^�X�V����
//				request.getRequestDispatcher("preEmpManager").forward(request, response);
//				session.setAttribute("edit_message", "�]�ƈ��f�[�^�X�V���������܂���");
//			}else{
////				�f�[�^�X�V���s
//				request.getRequestDispatcher("preEmpManager").forward(request, response);
//				session.setAttribute("edit_message", "�]�ƈ��f�[�^�X�V�����s���܂���");
//			}
		}catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}

	}

}
