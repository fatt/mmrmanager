package jp.co.mmr.servlet.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.PurchaseDAO;
import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.EmpDTO;
import jp.co.mmr.dto.PurchaseDTO;
import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.dto.RentalInfoDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

@WebServlet("/rentalDetailLending")
public class RentalDetailLendingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		RentalDTO rentalDto = (RentalDTO)session.getAttribute("rentalInfo");
		EmpDTO empDto = (EmpDTO)session.getAttribute("loginEmp");
		
		try(Connection con = DataSourceManager.getConnection()) {
			RentalDAO rentalDAO = new RentalDAO(con);
			PurchaseDAO purchaseDao = new PurchaseDAO(con);
			ArrayList<PurchaseDTO> purchaseList = purchaseDao.getPurchaseByRentalId(rentalDto.getId());
			int count = 0;
			for (int i = 0; i < purchaseList.size(); i++) {
				int originNum = Validate.convertStrToInteger(Validate.getStrByObject(request.getParameter(String.valueOf(i+1))));
				purchaseList.get(i).setOriginId(originNum);
				purchaseList.get(i).setReturnChargeId(empDto.getEmpId());
				purchaseList.get(i).setStatusId(RentalDetailServlet.STATUS_LEND);
				count += purchaseDao.setLendingParameter(purchaseList.get(i));
			}
			if (count == purchaseList.size()) {
				System.out.println("成功だぶーーーーーーーーーーー");
				rentalDto.setLendChargeId(empDto.getEmpId());
				rentalDto.setStatusId(RentalDetailServlet.STATUS_LEND);
				rentalDAO.setLendingParameter(rentalDto);
	            session.setAttribute("rentalInfo", rentalDto);
	            ArrayList<RentalInfoDTO> rentalDetailList = rentalDAO.getRentalDetailByRentalID(rentalDto.getId());
	            session.setAttribute("detailList", rentalDetailList);
			}
			// うまく更新できてない時はsessionに保存作業してないから元のページに戻るはず
            response.sendRedirect("rentalDetail");
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}
	}

}
