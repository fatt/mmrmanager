package jp.co.mmr.servlet.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.PurchaseDAO;
import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.EmpDTO;
import jp.co.mmr.dto.PurchaseDTO;
import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.dto.RentalInfoDTO;
import jp.co.mmr.util.DataSourceManager;

@WebServlet("/rentalDetailReturned")
public class RentalDetailReturnedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		RentalDTO rentalDTO = (RentalDTO)session.getAttribute("rentalInfo");
		EmpDTO empDto = (EmpDTO)session.getAttribute("loginEmp");

		try(Connection con = DataSourceManager.getConnection()) {
			RentalDAO rentalDAO = new RentalDAO(con);
			PurchaseDAO purchaseDAO = new PurchaseDAO(con);
			ArrayList<PurchaseDTO> purchaseList = purchaseDAO.getPurchaseByRentalId(rentalDTO.getId());
			int count = 0;
			for (PurchaseDTO purchase : purchaseList) {
				purchase.setStatusId(RentalDetailServlet.STATUS_RETURN);
				count += purchaseDAO.setReturnedParameter(purchase);
			}
			if (count == purchaseList.size()) {
				System.out.println("返却成功おぇーーーーーーーーーーーー");
				rentalDTO.setReturnChargeId(empDto.getEmpId());
				rentalDTO.setStatusId(RentalDetailServlet.STATUS_RETURN);
				java.util.Date date = new java.util.Date();
				Date today = new Date(date.getTime());
				rentalDTO.setReturnDate(today);
				rentalDAO.setReturnedParameter(rentalDTO);
				session.setAttribute("rentalInfo", rentalDTO);
	            ArrayList<RentalInfoDTO> rentalDetailList = rentalDAO.getRentalDetailByRentalID(rentalDTO.getId());
	            session.setAttribute("detailList", rentalDetailList);
			}
            response.sendRedirect("rentalDetail");
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}
		
	}

}
