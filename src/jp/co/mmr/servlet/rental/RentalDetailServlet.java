package jp.co.mmr.servlet.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.dto.RentalInfoDTO;
import jp.co.mmr.util.DataSourceManager;


@WebServlet("/rentalDetail")
public class RentalDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static int STATUS_RECEIPT = 1;
	final static int STATUS_LEND = 2;
	final static int STATUS_RETURN = 3;
	final static int STATUS_LEND_OVER = 4;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
	    HttpSession session = request.getSession();
	    RentalDTO rentalDto = (RentalDTO)session.getAttribute("rentalInfo");
	    
	    try(Connection con = DataSourceManager.getConnection()) {
	    	RentalDAO rentalDAO = new RentalDAO(con);
	    	int rentalStatus = rentalDto.getStatusId();
		    switch (rentalStatus) {
			case STATUS_RECEIPT:
			    // 受付状態(1)の場合
	            request.getRequestDispatcher("WEB-INF/jsp/rentalDetailReceipting.jsp").forward(request, response);
	            break;
			case STATUS_LEND:
	            // 貸出状態(2)の場合
	            request.getRequestDispatcher("WEB-INF/jsp/rentalDetailLending.jsp").forward(request, response);
	            break;
			case STATUS_RETURN:
			    // 返却状態(3)の場合
	            request.getRequestDispatcher("WEB-INF/jsp/rentalDetailReturned.jsp").forward(request, response);
				break;
			default:
				break;
			}
	    } catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}
	    
	    
	}
}

