package jp.co.mmr.servlet.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.dto.RentalInfoDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;


@WebServlet("/preRentalDetail")
public class PreRentalDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
		
	    Integer id = Validate.convertStrToInteger(Validate.getStrByObject(request.getParameter("rental_id")));
	    HttpSession session = request.getSession();
	    		
		try(Connection conn = DataSourceManager.getConnection()) {
			RentalDAO dao = new RentalDAO(conn);
			RentalDTO rentalDto = dao.getRentalById(id);
            ArrayList<RentalInfoDTO> rentalDetailList = dao.getRentalDetailByRentalID(rentalDto.getId());
            int sum = 0;
            for (RentalInfoDTO rental : rentalDetailList) {
                sum += rental.getPrice();
            }
            session.setAttribute("rentalInfo", rentalDto);
            session.setAttribute("detailList", rentalDetailList);
            session.setAttribute("priceSum", sum);
            
            response.sendRedirect("rentalDetail");
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}
	}
}

