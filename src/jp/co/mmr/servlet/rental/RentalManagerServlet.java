package jp.co.mmr.servlet.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.util.DataSourceManager;


@WebServlet("/rentalManager")
public class RentalManagerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        try(Connection conn = DataSourceManager.getConnection()){
            //  DAO繧帝�壹＠縺ｦDB縺ｸ縺ｮ繧ｳ繝阪け繧ｷ繝ｧ繝ｳ繧堤｢ｺ遶�
            RentalDAO dao = new RentalDAO(conn);

            //繝ｬ繝ｳ繧ｿ繝ｫ荳�隕ｧ縺ｸ騾√ｋ縺溘ａ縺ｮ繝ｪ繧ｹ繝医ｒ逕滓��
            ArrayList<RentalDTO> rentalInfo = new ArrayList<>();

            rentalInfo = dao.selectRentalInfo();                    

            request.setAttribute("rentalInfo", rentalInfo);
            //rentalManager.jsp縺ｸ驕ｷ遘ｻ
            request.getRequestDispatcher("WEB-INF/jsp/rentalManager.jsp").forward(request, response);

        }catch(SQLException e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }catch ( NamingException e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }
    }


    /**
     * 繝ｬ繝ｳ繧ｿ繝ｫ諠�蝣ｱ縺ｮ荳�隕ｧ讀懃ｴ｢
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {

    }
}

