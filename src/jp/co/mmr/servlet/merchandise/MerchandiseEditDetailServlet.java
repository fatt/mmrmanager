package jp.co.mmr.servlet.merchandise;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

@WebServlet("/merchandiseEditDetail")
public class MerchandiseEditDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try (Connection con = DataSourceManager.getConnection()) {
		    MerchandiseDAO dao = new MerchandiseDAO(con);
		    
		    Integer id = Validate.convertStrToInteger(Validate.getStrByObject(request.getParameter("merchandise_id")));
		    MerchandiseDTO dto = dao.getMerchandiseById(id);
		    
		    request.setAttribute("merchandise", dto);
		    request.getRequestDispatcher("/WEB-INF/jsp/merchandiseEditDetail.jsp").forward(request, response);

    	}catch(NumberFormatException | SQLException | NamingException e){
    		e.printStackTrace();
    	}
	}
}