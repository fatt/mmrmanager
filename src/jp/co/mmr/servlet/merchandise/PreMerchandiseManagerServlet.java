package jp.co.mmr.servlet.merchandise;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

@WebServlet("/preMerchandiseManager")
public class PreMerchandiseManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	    HttpSession session = request.getSession();
	    
	    try(Connection con = DataSourceManager.getConnection()) {
	    	
	    	Integer categoryId = Validate.convertStrToInteger(Validate.getStrByObject(request.getParameter("searchCategory")));
	        Integer junleId = Validate.convertStrToInteger(Validate.getStrByObject(request.getParameter("searchJunle")));
	        String name = Validate.getStrByObject(request.getParameter("searchName"));
	        
	        request.setAttribute("preCategory", categoryId);
	        request.setAttribute("preJunle", junleId);
	        request.setAttribute("preName", name);
	        
	        MerchandiseDAO merchandiseDao = new MerchandiseDAO(con);
	        ArrayList<MerchandiseDTO> merchandiseList = merchandiseDao.getAllMerchandise();
	        request.setAttribute("itemList", merchandiseList);
	        request.getRequestDispatcher("/WEB-INF/jsp/merchandiseManager.jsp").forward(request, response);
	    } catch (SQLException | NamingException e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }
	    
	    
	    
	}


}
