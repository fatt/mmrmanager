package jp.co.mmr.servlet.merchandise;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.util.DataSourceManager;

@WebServlet("/merchandiseAddDetail")
public class MerchandiseAddDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			    
	    try(Connection con = DataSourceManager.getConnection()) {
	        	    	
	        MerchandiseDAO dao = new MerchandiseDAO(con);
	        int id = dao.getNewId();
	        
	        request.setAttribute("id", id);
	        
	        request.getRequestDispatcher("/WEB-INF/jsp/merchandiseAddDetail.jsp").forward(request, response);
	    } catch (SQLException | NamingException e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }
	    
	    
	    
	}


}
