package jp.co.mmr.servlet.account;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.DataSourceManager;
import jp.co.mmr.dao.EmpDAO;
import jp.co.mmr.dto.EmpDTO;

@WebServlet("/password")
public class PasswordServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        //URLで連結された場合、ログイン画面に転送する
        response.sendRedirect("login.jsp");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        //パスワード変更画面から、新規パスワードと確認を取得する
        request.setCharacterEncoding("UTF-8");

        String newpassword1 = request.getParameter("newpassword1");
        String newpassword2 = request.getParameter("newpassword2");


        HttpSession session = request.getSession(false);
        EmpDTO dto = (EmpDTO)session.getAttribute("loginEmp");

        //DBとのconnectionをとる
        try(Connection conn = DataSourceManager.getConnection()) {
            EmpDAO dao = new EmpDAO(conn);
            //新規パスワードと確認用が違う場合
            if(!newpassword1.equals(newpassword2)){
                session.setAttribute("password_error_message", "新規パスワードと確認が一致していません");
                request.getRequestDispatcher("/WEB-INF/changePassword.jsp").forward(request, response);

                //新規パスワードと初期化パスワードが一致している場合
            }else if((newpassword1.equals(newpassword2)) && (newpassword1.equals(LoginServlet.prepassword))){
                session.setAttribute("password_error_message", "パスワードを変更してください");
                request.getRequestDispatcher("/WEB-INF/changePassword.jsp").forward(request, response);
            }else{
                dto.setEmpPassword(newpassword1);
                if(dao.changepassword(dto.getEmpId(), dto.getEmpPassword())){
//                    request.getRequestDispatcher("rentalManager").forward(request, response);
                    response.sendRedirect("rentalManager");
                }else{
                    session.setAttribute("password_error_message", "パスワード変更失敗しました");
                    request.getRequestDispatcher("/WEB-INF/changePassword.jsp").forward(request, response);
                }
            } 
        }catch (SQLException | NamingException e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }
    }
}
