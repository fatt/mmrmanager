package jp.co.mmr.servlet.account;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
			
			// セッションを取得する
			HttpSession session = request.getSession(false);
			
			//　セッションを破棄する
			if (session != null) {
				session.invalidate();
			}
			
			//　ログイン画面に遷移する
			response.sendRedirect("login.jsp");
		}

}
