package jp.co.mmr.servlet.account;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.DataSourceManager;
import jp.co.mmr.dao.EmpDAO;
import jp.co.mmr.dto.EmpDTO;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    static String prepassword = "mmr12345";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String id = request.getParameter("id");
        String password = request.getParameter("password");
        try(Connection conn = DataSourceManager.getConnection()) {
            EmpDAO dao = new EmpDAO(conn);
            HttpSession session = request.getSession();
            Pattern p = Pattern.compile("^[0-9a-zA-Z]+$");
            Matcher checkid = p.matcher(id);
            Matcher checkpassword = p.matcher(password);
            if (!dao.selectByIdAndPassword(id , password)){
                session.setAttribute("error_message",  "ユーザID又はパスワードが間違っています");
                doGet (request, response);
            }else if(id.length() > 20 || password.length() > 20){
                session.setAttribute("error_message", "IDとパスワードは20桁内で入力してください");
                doGet (request, response);
            }else if(!checkid.find() || !checkpassword.find()){
                session.setAttribute("error_message", "IDとパスワードにはローマ字と数字だけ入力してください");
            }
            else{
                session.removeAttribute("error_message");
                EmpDTO dto = dao.getEmpById(id);
                session.setAttribute("loginEmp", dto);
                if(password.equals(prepassword)){
                    request.getRequestDispatcher("/WEB-INF/changePassword.jsp").forward(request, response);
                    return;
                }else{
                    response.sendRedirect("rentalManager");
                }
            }
        }catch (SQLException | NamingException e) {
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        }
    }
}
